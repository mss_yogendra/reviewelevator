<?php
App::uses('AppModel', 'Model');
class Partner extends AppModel {
var $virtualFields = array('agencyname'=>'SELECT agencyname FROM agencysite_settings ur WHERE ur.user_id = Partner.user_id');		
	public $actsAs = array('Containable');
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)

	);

	public function beforeSave($options = array()) {
		   if(isset($this->data['Partner']['agencyIds']) && !empty($this->data['Partner']['agencyIds'])){
		   		$this->data['Partner']['agencyIds'] = serialize($this->data['Partner']['agencyIds']);
		   }
	}

	public function afterFind($results, $primary = false) {
		if(!empty($results)){
			foreach ($results as $key => $value) {
				if(isset($value['Partner']['agencyIds']) && !empty($value['Partner']['agencyIds'])){
					$results[$key]['Partner']['agencyIds']=unserialize($value['Partner']['agencyIds']);
				}else{
					continue;
				}
			}
		}
		return $results;
	}
}
