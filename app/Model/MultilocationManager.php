<?php
App::uses('AppModel', 'Model');
class MultilocationManager extends AppModel {
var $virtualFields = array('parentagencyname'=>'SELECT agencyname FROM agencysite_settings ur WHERE ur.user_id = MultilocationManager.agency_id');
public $actsAs = array('Containable');
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
			
		)
	);
}
