<?php
App::uses('AppModel', 'Model');

class BusinessManager extends AppModel {
	public function beforeSave($options = array()) {
		   if(isset($this->data['BusinessManager']['businessIds'])){
		   		$this->data['BusinessManager']['businessIds'] = serialize($this->data['BusinessManager']['businessIds']);
		   }
	}
var $virtualFields = array('agencyname'=>'SELECT agencyname FROM agencysite_settings ur WHERE ur.user_id = BusinessManager.agency_id');	
public $actsAs = array('Containable');


	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function afterFind($results, $primary = false) {
		if(!empty($results)){
			foreach ($results as $key => $value) {
				if(isset($value['BusinessManager']['businessIds']) && @$value['BusinessManager']['businessIds']){
					$results[$key]['BusinessManager']['businessIds']=unserialize($value['BusinessManager']['businessIds']);
				}else{
					continue;
				}
			}
		}
		return $results;
	}

}
