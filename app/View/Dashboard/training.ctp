<?php echo $this->element('nav')?>
<!-- BEGIN CONTENT -->
<?php echo $this->Session->flash(); ?>
<div class="page-content-wrapper">
	<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		   <?php echo $this->Session->flash(); ?>  
		  <h3 class="page-title">
		  <?php echo $design['AgencysiteSetting']['sitetitle']; ?> - Training
		  </h3>
		  <div class="page-bar">
			<ul class="page-breadcrumb">
			  <li>
				<i class="fa fa-home"></i>
				<a href="<?php echo HTTP_ROOT?>">Home</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
				<a href="<?php echo HTTP_ROOT?>dashboard/addTraining">Edit Training</a>
			  </li>
			</ul>
			<div class="page-toolbar">
			  
			</div>
		  </div>
		  <!-- END PAGE HEADER-->
        
			
			<div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-lg-10 col-md-9 col-sm-8">
				<?php echo $this->Session->flash(); ?>  
			  <!--@@@ @@@ @@@ @@@ @@@ @@@ -->
		      <!--   >>  BEGIN PORTLET  << -->
			  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
				<?php 
				if(isset($design) && !empty($design['AgencysiteSetting']['sitebackgroundcolor']))
					{
						$barcolor = $design['AgencysiteSetting']['sitebarcolor'];  
				?>

				<!-- BEGIN HEADER -->

					<div class="portlet box" style="background-color:#<?php print ("$barcolor"); ?>">

				<?php } else { ?>
					
					<div class="portlet box reforce-red">
					  
				<?php } ?>
					  
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>Training
						  </div>
						  <div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->
						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
							
							  
											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
									 <a href="<?php echo HTTP_ROOT?>dashboard/addTraining">Edit content</a>
									 </br></br />
								Add Reach Local Training Content Here
									<!-- MAIN END -->
								</div>
							</div>
							
						</div>
						<!-- END PORTLET BODY -->
						
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					<!--   >>   END PORTLET  <<  -->
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
			  <!----------------------------------->
			  <!----------------------------------->
		
				</div>
			<?php echo $this->element('reviewsidebar')?>
			</div> <!-- END LAYOUT ROW -->	
		
	</div>
</div>	


<!--	<div class="col-md-12">
										
										 <?php if(count($trainings)>0):?>
											<?php foreach ($trainings as $_training):?>
								      <a href="<?php echo HTTP_ROOT?>dashboard/addTraining">Edit content</a>
								<div class="row training-row">

								  <h2 class="training-title"><?php echo $_training['Training']['title'];?></h2>
											  <?php if(!empty($_training['Training']['url'])):?>
											  
											  <iframe style="width:250px; height:250px; float:left " src="<?php echo $_training['Training']['url'];?>" frameborder="0"rel=0  allowfullscreen></iframe>
									  
												<?php else:?>
													   <?php if(!empty($_training['Training']['url']));?>
												 <?php endif;?>
								 <p><?php echo @$_training['Training']['description'];?></p>				 

								</div>
												<?php endforeach;?>
											<?php else:?>
												There are no content available right now.
												<a href="<?php echo HTTP_ROOT?>dashboard/addTraining">Add content</a>
											<?php endif;?>
										
									</div>
									-->
