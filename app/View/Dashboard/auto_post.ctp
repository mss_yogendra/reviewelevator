<?php echo $this->element('nav_business_user')?>
<!-- BEGIN CONTENT -->
<?php echo $this->Session->flash(); ?>
<div class="page-content-wrapper">
	<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		   <?php echo $this->Session->flash(); ?>  
		  <h3 class="page-title">
		  <?php echo $this->element('welcome')?> 
		  </h3>
		  <div class="page-bar">
			<ul class="page-breadcrumb">
			  <li>
				<i class="fa fa-home"></i>
				<a href="<?php echo HTTP_ROOT?>">Home</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
				<a href="#">Review Auto Post</a>
			  </li>
			</ul>
			<div class="page-toolbar">
			  
			</div>
		  </div>
		  <!-- END PAGE HEADER-->
        
			
			<div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-lg-10 col-md-9 col-sm-8">
				<?php echo $this->Session->flash(); ?>  
				
				
				<?php 
				if(isset($design) && !empty($design['AgencysiteSetting']['sitebackgroundcolor']))
					{
						$barcolor = $design['AgencysiteSetting']['sitebarcolor'];  
				?>

				<!-- BEGIN HEADER -->

					<div class="portlet box" style="background-color:#<?php print ("$barcolor"); ?>">

				<?php } else { ?>
					
					<div class="portlet box reforce-red">
					  
				<?php } ?>
					  
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>Facebook
						  </div>
						  <div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->
						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
									<div class="col-md-12">
										<ul>
											<!-- <a id="login-share" href="javascript:;" onclick='FBLogin(); return false;'><li>Login to auto post on facebook</li></a>
 -->
											<div class="btn-group" style="margin-bottom:20px;">
							                      <button class="btn green" onclick='FBLogin(); return false;'>
							                      	Connect with facebook 
							                      </button>
							                </div>
							                </br>
										   <div id="login-status">
							                  <!-- <a href="javascript:;" onclick='PagePost(); return false;'>Click here to post on facebook</a> -->
							                  	<form method="post" id="fbautopost" action="<?php echo HTTP_ROOT?>dashboard/FbAutopost/<?php echo $busid ?>">
							                  	    <?php if(isset($facebook['ReviewAutopost']['id'])){?>
							                  	    	<input type="hidden" name="data[ReviewAutopost][id]" id="Id" value="<?= $facebook['ReviewAutopost']['id'] ?>"/>
							                  	    <?php }?>
													<input type="hidden" name="data[ReviewAutopost][pageId]" id="pageId" value=""/>
													<input type="hidden" name="data[ReviewAutopost][token]" id="exchangeToken" value=""/>
													<input type="hidden" name="data[ReviewAutopost][socialmedia]" id="socialmedia" value="facebook"/>
													<input type="hidden" name="data[ReviewAutopost][pageName]" id="pagename" value="facebook"/>
											    </form>  
							                </div>
							                 <?php if(isset($facebook['ReviewAutopost']['id'])){?>
								                <table class="table table-bordered table-striped">
								                	<tr>
								                		<td>Status</td>
								                	<?php $target = array(0=>1,1=>0); ?>
													<?php if($facebook['ReviewAutopost']['status'] == 1) {?>
													<td> 
													  <div style="cursor:pointer" class='statuschange' id="change<?php echo $facebook['ReviewAutopost']['id']?>" rel="<?php echo HTTP_ROOT.'dashboard/updateAutopost/'.base64_encode($facebook['ReviewAutopost']['id']).'/'.$target[$facebook['ReviewAutopost']['status']]?>">
													  <img id="imge<?php echo $facebook['ReviewAutopost']['id']?>" src="<?php echo HTTP_ROOT?>img/green_dots.png" width="25" height="11"></div>
													</td>
													<?php }else{ ?>
													<td> 
													  <div style="cursor:pointer" class='statuschange' id="change<?php echo $facebook['ReviewAutopost']['id']?>" rel="<?php echo HTTP_ROOT.'dashboard/updateAutopost/'.base64_encode($facebook['ReviewAutopost']['id']).'/'.$target[$facebook['ReviewAutopost']['status']]?>">
													  <img id="imge<?php echo $facebook['ReviewAutopost']['id']?>" src="<?php echo HTTP_ROOT?>img/red-dots.png" width="25" height="11"></div>
													</td>
													<?php  }?>
								                	</tr>
								                	<tr><td>Page Name</td><td><?= $facebook['ReviewAutopost']['pageName'] ?></td></tr>
								                	<tr><td>Page Id</td><td><?= $facebook['ReviewAutopost']['pageId'] ?></td></tr>
								                </table>
								              <?php }?>  
										</ul>
									</div>
									<!-- MAIN END -->
								</div>
							</div>
							
						</div>
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					
				</div>
			</div> <!-- END LAYOUT ROW -->	


		    <div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-lg-10 col-md-9 col-sm-8">
				<?php 
				if(isset($design) && !empty($design['AgencysiteSetting']['sitebackgroundcolor']))
					{
						$barcolor = $design['AgencysiteSetting']['sitebarcolor'];  
				?>

				<!-- BEGIN HEADER -->

					<div class="portlet box" style="background-color:#<?php print ("$barcolor"); ?>">

				<?php } else { ?>
					
					<div class="portlet box reforce-red">
					  
				<?php } ?>
					  
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>Twitter
						  </div>
						  <div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->
						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
									<div class="col-md-12">
										<ul>
											<!-- <a id="login-share" href="javascript:;" onclick='FBLogin(); return false;'><li>Login to auto post on facebook</li></a>
 -->
											<div class="btn-group" style="margin-bottom:20px;">
							                      <button class="btn green" id="connect" onclick='return false;'>
							                      	Connect with twitter 
							                      </button>
							                </div>
							                </br>
										   
							                 <?php if(isset($twitter['ReviewAutopost']['id'])){?>
								                <table class="table table-bordered table-striped">
								                	<tr>
								                		<td>Status</td>
								                	<?php $target = array(0=>1,1=>0); ?>
													<?php if($twitter['ReviewAutopost']['status'] == 1) {?>
													<td> 
													  <div style="cursor:pointer" class='statuschange' id="change<?php echo $twitter['ReviewAutopost']['id']?>" rel="<?php echo HTTP_ROOT.'dashboard/updateAutopost/'.base64_encode($twitter['ReviewAutopost']['id']).'/'.$target[$twitter['ReviewAutopost']['status']]?>">
													  <img id="imge<?php echo $twitter['ReviewAutopost']['id']?>" src="<?php echo HTTP_ROOT?>img/green_dots.png" width="25" height="11"></div>
													</td>
													<?php }else{ ?>
													<td> 
													  <div style="cursor:pointer" class='statuschange' id="change<?php echo $twitter['ReviewAutopost']['id']?>" rel="<?php echo HTTP_ROOT.'dashboard/updateAutopost/'.base64_encode($twitter['ReviewAutopost']['id']).'/'.$target[$twitter['ReviewAutopost']['status']]?>">
													  <img id="imge<?php echo $twitter['ReviewAutopost']['id']?>" src="<?php echo HTTP_ROOT?>img/red-dots.png" width="25" height="11"></div>
													</td>
													<?php  }?>
								                	</tr>
								                	<tr><td>Screen Name</td><td><?= $twitter['ReviewAutopost']['pageId'] ?></td></tr>
								                	
								                </table>
								              <?php }?>  
										</ul>
									</div>
									<!-- MAIN END -->
								</div>
							</div>
							
						</div>
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					
				</div>
			</div> <!-- END LAYOUT ROW -->	

			<?php //echo $this->element('reviews_business_user')?> 
	</div>
</div>	
<script src='http://connect.facebook.net/en_US/all.js'></script>
<script> 
FB.init({ appId: "<?php echo Fb_App_Id;?>",
                    status: true,
                    cookie: true,
                    xfbml: true,
                    oauth: true});

function FBLogin(){  
FB.login(function(response) {
    if(response.status=="connected"){
      //$("#login-share").hide();
      FB.api('/'+response.authResponse.userID+'/accounts', 'get', {}, function(response) {
        //console.log();
         $("#sm_pages").empty();
         var x = document.getElementById("sm_pages");
         var option = document.createElement("option");
          option.text = "Select Page";
          option.value = "";
          x.add(option);
          for(i=0;i<response.data.length;i++){
            // console.log(response.data.length);
               var x = document.getElementById("sm_pages");
               var option = document.createElement("option");
              option.text = response.data[i].name;
              option.value = response.data[i].access_token+'pageid'+response.data[i].id;
              if(i==0){
                option.selected="selected";
              }
              x.add(option);
          }
      
      });
     $("#login-status").show();
     $('#autopostModel').click(); 
    }else{
      $("#login-status").hide(); 
      //$("#login-share").show();
    }  
}, {scope:'manage_pages,publish_pages,publish_actions'});
}
</script>

<!-- Start Page Pop up -->
<a id="autopostModel" style="cursor:pointer;display:none" data-toggle="modal" data-target="#autopostReview">
</a>   

<div class="modal fade" id="autopostReview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             <h4 id="revby">Select Facebook Page</h4>   
          </div>
           <div class="modal-body add_cc">
                <div class="form-group" id="pages">
					  <div class="col-sm-4">
					  <select class="form-selected form-control" id="sm_pages">
						  <option value="">Select Page</option>
					</select>
					</div>
				</div>
				<!-- <div id="autopost" style="cursor:pointer;">Save Page</div>     -->
				<button class="submit btn btn-primary confirm yellow" id="autopost" style="margin-top:-14px"> 
            Submit Page
        </button>
          </div>  
          <div class="modal-footer">
            <button type="button" class="btn btn-default buttonclose close_modal" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
</div>

<!-- End Page Pop up -->
<a id="alertModel" style="cursor:pointer;display:none" data-toggle="modal" data-target="#alertMsg">
</a>   

<div class="modal fade" id="alertMsg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
           <div class="modal-body add_cc">
                <div class="form-group" id="msgs">
					 
				</div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default buttonclose close_modal" data-dismiss="modal">Close</button>
          </div>  
        </div>
      </div>
</div>
<script type="text/javascript">
	$('#autopost').click(function(){
	   var params=document.getElementById("sm_pages").value.split('pageid');
	   var pagename=$("#sm_pages :selected").text();
	   var token=params[0];
	   var pageid=params[1];
	   if(token){
	      $('#exchangeToken').val(token);
	      $('#pagename').val(pagename);
	      $('#pageId').val(pageid);
	      $('#fbautopost').submit();
	   }else{
	      $('#msgs').html("Please select a facebook page.");                  
		  $('#alertModel').click();
	     return false;
	   }
	});

	$('.statuschange').on('click', function(){
	   var url=$(this).attr('rel');
	   $("#loading").show();
	   $.ajax({                   
				url: url,
				type: 'POST',
				 cache: false,
				dataType: 'json',
				success: function (response) {
				  if(response[0]=='success'){
					 var newurl=ajax_url+'dashboard/updateAutopost/'+response[1]+'/'+response[2];
					   if(response[2]==0){
						  var newdots=ajax_url+'img/green_dots.png';
					   }else{
						 var newdots=ajax_url+'img/red-dots.png';
					   }
					   $('#change'+response[3]).attr('rel',newurl);
					   $('#imge'+response[3]).attr('src',newdots);
					   $("#loading").hide(); 
					   $('#msgs').html('Status has been updated Successfully.');                  
					   $('#alertModel').click();
				  }else if(response[0]=='expire'){
				  	$("#loading").hide();
				  	window.location.reload();
				  }else{
					$("#loading").hide();
					$('#msgs').html("!Oops there is something wrong with server.Please try again.");                  
					$('#alertModel').click();
				  }
				  
				  }
			})

	});
</script>

<div id="loading" style="display:none">
  <img id="loading-image" src="<?php echo HTTP_ROOT?>img/ajax-loader.gif" alt="Loading..." />
</div>
<style type="text/css">
#loading {
  
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  position: fixed;
  display: block;
  opacity: 0.7;
  background-color: #fff;
  z-index: 99;
  text-align: center;
}

#loading-image {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 100;
}
.modal-header .close {
	margin-top: -11px;
}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		
    $('#connect').click(function(){
    	var param="<?php echo $busid?>";
    	if(param){
    		var id=param.split('bussiness');
    		<?php $bid="" ;?>
    	}else{
    		var id=new Array();
             <?php 
               if($this->Session->read('Auth.User.usertype')=="subscriber"){
               		$bid=base64_encode($this->Session->read('Auth.User')['Business']['id']);
               }
             ?>
            var bid="<?php echo $bid ?>";
            if(bid){
            	id[1]=bid;
            }else{
            	id[1]='';
            }
    		
    	}
        $.oauthpopup({
            path: ajax_url+"twitter/twitter/connect/"+id[1],
            callback: function(){
                window.location.reload();
            }
        });
    });
});
(function($){
    $.oauthpopup = function(options)
    {
        if (!options || !options.path) {
            throw new Error("options.path must not be empty");
        }
        options = $.extend({
            windowName: 'ConnectWithOAuth' // should not include space for IE
          , windowOptions: 'location=0,status=0,width=800,height=400'
          , callback: function(){ window.location.reload(); }
        }, options);

        var oauthWindow   = window.open(options.path, options.windowName, options.windowOptions);
        var oauthInterval = window.setInterval(function(){
            if (oauthWindow.closed) {
                window.clearInterval(oauthInterval);
                options.callback();
            }
        }, 1000);
    };

    $.fn.oauthpopup = function(options) {
        $this = $(this);
        $this.click($.oauthpopup.bind(this, options));
    };
})(jQuery);

</script>