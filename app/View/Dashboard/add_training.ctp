<?php echo $this->element('nav')?>
<!-- BEGIN CONTENT -->
<?php echo $this->Session->flash(); ?>
<div class="page-content-wrapper">
	<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		   <?php echo $this->Session->flash(); ?>  
		  <h3 class="page-title">
		  <?php echo $design['AgencysiteSetting']['sitetitle']; ?> - Training
		  </h3>
		  <div class="page-bar">
			<ul class="page-breadcrumb">
			  <li>
				<i class="fa fa-home"></i>
				<a href="<?php echo HTTP_ROOT?>">Home</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
				<a href="<?php echo HTTP_ROOT?>dashboard/addTraining">Edit Training</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
			  	 	<a href="<?php echo HTTP_ROOT ?>dashboard/training"></i>Go Back</a>
			  	</li> 	
			</ul>
			<div class="page-toolbar">
			  
			</div>
		  </div>
		  <!-- END PAGE HEADER-->
        
			
			<div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-lg-10 col-md-9 col-sm-8">
				<?php echo $this->Session->flash(); ?>  
			  <!--@@@ @@@ @@@ @@@ @@@ @@@ -->
		      <!--   >>  BEGIN PORTLET  << -->
			  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
				<?php 
				if(isset($design) && !empty($design['AgencysiteSetting']['sitebackgroundcolor']))
					{
						$barcolor = $design['AgencysiteSetting']['sitebarcolor'];  
				?>

				<!-- BEGIN HEADER -->

					<div class="portlet box" style="background-color:#<?php print ("$barcolor"); ?>">

				<?php } else { ?>
					
					<div class="portlet box reforce-red">
					  
				<?php } ?>
					  
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>Edit Training
						</div>

						 <div class="tools">

							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->

						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
							
							  
											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
						<div class="col-md-12">
						<a href="<?php echo HTTP_ROOT ?>dashboard/training"></i>Go Back</a>	
						 <form class="form-horizontal" accept-charset="utf-8" method="post" id="agencyTraining" action="<?php echo HTTP_ROOT?>dashboard/addTraining">	
					     	<div class="form-group"> 
	                          <label class="control-label col-sm-4" for="email">Title</label> 
	                           <div class="col-sm-7">           
	                            <input class="form-control account-back" type="text" id="training-title" name="data[Training][title]" value="<?php echo @$data['Training']['title']?>">
	                            </div>
	                            <input type="hidden" name="data[Training][id]" value="<?php echo @$data['Training']['id'] ?>">
                        	</div>

                        	<div class="form-group"> 
	                          <label class="control-label col-sm-4" for="email">URL</label> 
	                           <div class="col-sm-7">           
	                            <input class="form-control account-back" type="text" id="training-url" name="data[Training][url]" value="<?php echo @$data['Training']['url']?>">
	                           
	                            <p>Ex:(http://www.youtube.com/watch?v=VRlmCf1Kj2o)</p>
	                             </div>
                        	</div>	

                          <div class="form-group">
                           <label class="control-label col-sm-4" for="email">Vedio Description:</label>
                           <div class="col-sm-7">  
                           <textarea name="data[Training][description]" id="cktext"><?php echo @$data['Training']['description']?></textarea>
                          <?php if(isset($error['description'])) { ?><p class="error"><?php echo $error['description']; ?></p><?php } ?>
                           </div>
                         </div>
                           <div class="form-group">
                        <label class="control-label col-sm-4" for="email">&nbsp;</label>
                        <div class="col-sm-4 submitting">
                        <input type="submit" class="submit btn btn-primary" value="Submit">
                        </div>
                        </div>

                    </form>	
										 
								
								 
										
									</div>
									<!-- MAIN END -->
								</div>
							</div>
							
						</div>
						<!-- END PORTLET BODY -->
						
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					<!--   >>   END PORTLET  <<  -->
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
			  <!----------------------------------->
			  <!----------------------------------->
		
				</div>
			<?php echo $this->element('reviewsidebar')?>
			</div> <!-- END LAYOUT ROW -->	
		
	</div>
</div>	
<script type="text/javascript">
   window.onload = function()
   {
      CKEDITOR.replace( 'cktext' , {
        resize_enabled: false,
        removeButtons: 'Save,NewPage,Preview,Templates',
        width: '100%'});
      CKEDITOR.replace( 'cktext1', {
        resize_enabled: false,
        removeButtons: 'Save,NewPage,Preview,Templates',
        width: '100%' });

      CKEDITOR.instances.cktext.on('contentDom', function() {
        CKEDITOR.instances.cktext.document.on('keyup', function(event) {
           if(CKEDITOR.instances.cktext.getData()==''){
                $("[for='cktext']").css('display','block');
                $('#cktext').removeClass('valid');
                $('#cktext').addClass('error');
                $('#cktext').next().css('border','1px solid red');
           }else{
                $("[for='cktext']").css('display','none');
                $('#cktext').removeClass('error');
                $('#cktext').addClass('valid');
                $('#cktext').next().css('border','none');
           }
        });
    });

   
   };
   </script>
   <script type="text/javascript">
$(document).ready(function(){
    $('#agencyTraining').validate({
        onfocusout: function (element) {
             $(element).valid();
            },
            rules:
            {
                "data[Training][url]":
                {   
                	required:true,
                	url:true
                },
                "data[Training][title]":
                {
                	required:true,
                	accept: "[a-zA-Z]+"
                },
                "data[Training][description]":
                {
                	required:true
                },


            },
            messages:
            {
                "data[Training][url]":
                {
                  required:'Please enter the url',	
                  url:'Please enter valid url.'
                },
                "data[Training][title]":
                {
                 required:'Please enter the title',
                 accept:'Please enter the valid title'
                },
                "data[Training][description]":
                {
                 required:'Please enter the vedio description' 
                },

            }
        
        
        });
});
</script>
