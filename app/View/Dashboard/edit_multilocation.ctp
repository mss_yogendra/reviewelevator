<script type="text/javascript">
$(document).ready(function(){
    $('#MultilocationManagerForm').validate({
          onfocusout: function (element) {
             $(element).valid();
            },
            rules:
            {
               
                "data[User][firstname]":
                {
                     required:true,
                     accept: "[a-zA-Z]+",
		            maxlength:20
                },
                "data[User][lastname]":
                {
                     required:true,
                     accept: "[a-zA-Z]+",
		             maxlength:20
                },
               "data[User][email]":
                {
                    required:true,
                    email:true,
                    remote:ajax_url+"admin/checkExistEmail/<?php echo $data['User']['id']?>"
                },
                 "data[agency][agencyname]":
                {
                  required:true
                },
                
            },
            messages:
            {
            	"data[agency][user_id]":
                {
                    required:"Please Enter The First Name",
               
                },
                "data[User][firstname]":
                {
                    required:"Please Enter The First Name",
                     accept :"Please enter only characters "
                },
                 "data[User][email]":
                {
                    required:'Please enter email.',
                    email:'Please enter valid email.',
                    remote:'Email id is already exist.'
                  
                },
                "data[User][lastname]":
                {
                     required:"Please Enter The Last Name",
                     accept :"Please enter only characters "
                },
               "data[agency][agencyname]":
                {
                  required:"Please Enter The Agency Name"
                },
                
                
            }
        
        
        });
        
    
    }); 
</script> 
<?php echo $this->element('nav')?>
<!-- BEGIN CONTENT -->
<?php echo $this->Session->flash(); ?>
<div class="page-content-wrapper">
	<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		   <?php echo $this->Session->flash(); ?>  
		  <h3 class="page-title">
		   Edit Multilocation Manager 
		  </h3>
		  <div class="page-bar">
			<ul class="page-breadcrumb">
			  <li>
				<i class="fa fa-home"></i>
				<a href="<?php echo HTTP_ROOT?>">Home</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
				<a href="#">Multilocation Manager</a>
			  </li>
			</ul>
			<div class="page-toolbar">
			  
			</div>
		  </div>
		  <!-- END PAGE HEADER-->
        
			
			<div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-sm-12">
				<?php echo $this->Session->flash(); ?>  
				
					  <div class="portlet box reforce-red">
					  
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>edit Manager
						  </div>
						  <div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->
						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
							
								<div class="col-sm-12">
									 <span><a class="go-back btn btn-primary" href="<?php echo HTTP_ROOT?>dashboard/agencyManagers">Go Back</a></span>
								</div>

											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
									<div class="">
									
								 <form class="form-horizontal"  accept-charset="utf-8" method="post" id="MultilocationManagerForm" action="<?php echo HTTP_ROOT?>dashboard/editMultilocation">

								 	<input type="hidden" name="data[User][id]" value="<?php echo @$data['User']['id']?>" />


								 	<input type="hidden" name="data[AgencysiteSetting][id]" value="<?php echo @$data['AgencysiteSetting']['id']?>" />
                                    
								   <div class="form-group">
									  <label class="control-label col-sm-4" >First Name:</label>
									  <div class="col-sm-4">
										<input type="text" class="form-control form-back" placeholder="Enter First Name" name="data[User][firstname]" value="<?php echo @$data['User']['firstname']?>">
									  </div>
									</div>


									<div class="form-group">
									  <label class="control-label col-sm-4" >Last Name:</label>
									  <div class="col-sm-4">
										<input type="text" class="form-control form-back" placeholder="Enter Last Name" name="data[User][lastname]" value="<?php echo @$data['User']['lastname'];?>">
										
									  </div>
									</div>

								 <div class="form-group">
									  <label class="control-label col-sm-4" >Agency Name:</label>
									  <div class="col-sm-4">
										<input type="text" class="form-control form-back" placeholder="Enter Agency Name" name="data[AgencysiteSetting][agencyname]" value="<?php echo @$data['User']['agencyname'];?>">
									  </div>
									</div>

									 <div class="form-group">
									  <label class="control-label col-sm-4" >Email:</label>
									  <div class="col-sm-4">
										<input type="email" id="emails"name="data[User][email]" placeholder="Enter email" class="form-control form-back" value="<?php echo @$data['User']['email'];?>">
									  </div>
									</div>

								  <div class="form-group">
									<label class="control-label col-sm-4" for="email">&nbsp;</label>  
									<div class="col-sm-8 submitting"> 
										<input type="submit" class="submit btn btn-primary" value="Submit">
									</div> 
								  </div>
								</form>
							</div>
									<!-- MAIN END -->
								</div>
							</div>
						</div>
						<!-- END PORTLET BODY -->
						
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					<!--   >>   END PORTLET  <<  -->
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
			  <!----------------------------------->
			  <!----------------------------------->
		
				</div>
			</div> <!-- END LAYOUT ROW -->	
		
	</div>
</div>	

