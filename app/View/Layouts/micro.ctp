<?php


$cakeDescription = __d('cake_dev', 'Reputation Management System');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php echo $this->Html->script('jquery'); ?>
	<?php echo $this->Html->script('jquery.validate.min'); ?>
	<script type="text/javascript"> 
		</script>

	<!-- js files -->
	<?php echo $this->Html->script('jquery.min.js'); ?>
	<?php echo $this->Html->script('bootstrap.js'); ?>
	<?php echo $this->Html->script(array('common.js','custom.js'));?>
	<?php echo $this->Html->script('jquery.validate.js'); ?>
	<?php echo $this->Html->script('ckeditor/ckeditor.js'); ?>
	<!-- stylesheets -->

	<?php echo $this->Html->css('bootstrap.min.css'); ?>
	<?php echo $this->Html->css('repelev.css'); ?>
	<?php echo $this->Html->css('bootstrap-theme.min.css'); ?>

</head>
<body>
<div id="header">
			

				
				<?php if(isset($design) && !empty($design['AgencysiteSetting']['sitebackgroundcolor']))
					{
			 $headcolor = $design['AgencysiteSetting']['siteheadcolor'];  
				   ?>
			<!-- BEGIN HEADER -->
			  <div class="navbar" id="logoh" style="background-color:#<?php print ("$headcolor"); ?>">

				<?php } else { ?>
				
				<div class="navbar" id="logoh">
				  <?php } ?>
			

				</div>
				
			</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>
			
			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			
		</div>
	</div>
	
</body>
</html>
