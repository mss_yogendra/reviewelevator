        <div class="col-md-3 col-sm-4">
          <!-- Total Feedback Box -->
          				<?php 
				if(isset($design) && !empty($design['AgencysiteSetting']['sitebackgroundcolor']))
					{
						$headcolor = $design['AgencysiteSetting']['siteheadcolor'];  
				?>

				<!-- BEGIN HEADER -->

					<div class="dashboard-stat" style="background-color:#<?php print ("$headcolor"); ?>">

				<?php } else { ?>
					
					<div class="dashboard-stat blue">
					  
				<?php } ?>
            <div class="visual">
              <i class="fa fa-comments"></i>
            </div>
            
              <div class="rateavg">
                 <?php if($businessesdata['Business']['averageRating']>0){?>
				<span><?php echo number_format((float)$businessesdata['Business']['averageRating'], 2, '.', '');?></span>
				<?php } else {?>
				   <span><?php echo number_format((float)0, 2, '.', '');?></span>
				<?php }?>
              </div>
              <div class="details dside">
              
              <div class="desc">
                 Your Feedback Rating
              </div>
              <div class="more">
            <?php if($businessesdata['Business']['totalReviews']>0) {?>
			<?php echo 'Overall feedback rating out of '.$businessesdata['Business']['totalReviews']?>
			<?php }?>              
			  </div>
			  </div>
                 
          </div>
        
		
			
				<?php 
				if(isset($design) && !empty($design['AgencysiteSetting']['sitebackgroundcolor']))
					{
						$barcolor = $design['AgencysiteSetting']['sitebarcolor'];  
				?>

				<!-- BEGIN HEADER -->

					<div class="dashboard-stat" style="background-color:#<?php print ("$barcolor"); ?>">

				<?php } else { ?>
					
					<div class="dashboard-stat reforce-red">
					  
				<?php } ?>
             <div class="stars-title">
                 Your Star Ratings
              </div>
			  <div class="star-chart-total">
            <?php if($businessesdata['Business']['totalReviews']>0){?>
				<?php echo $businessesdata['Business']['totalReviews'].' Reviews';}else {?>  
				<?php echo "Reviews";?> 
			<?php }?>
              </div>
                 <?php if($businessesdata['Business']['totalReviews']>0){?>
			<ul class="breakdown-stars">
				<li><span><?php echo $businessesdata['Business']['fivestarCount']?></span><img src="<?php echo HTTP_ROOT?>app/webroot/img/5stars.png"</li>
				<li><span><?php echo $businessesdata['Business']['fourstarCount']?></span><img src="<?php echo HTTP_ROOT?>app/webroot/img/4stars.png"</li>
				<li><span><?php echo $businessesdata['Business']['threestarCount']?></span><img src="<?php echo HTTP_ROOT?>app/webroot/img/3stars.png"</li>
				<li><span><?php echo $businessesdata['Business']['twostarCount']?></span><img src="<?php echo HTTP_ROOT?>app/webroot/img/2stars.png"</li>
				<li><span><?php echo $businessesdata['Business']['onestarCount']?></span><img src="<?php echo HTTP_ROOT?>app/webroot/img/1star.png"</li>
			</ul>
			<?php }else{?>
     				<span><?php echo "NO FEEDBACK YET...";?></span> 
			<?php }?>
              
              <div class="details dside">
              
              <div class="desc">
                 
              </div>
              <div class="more-chart more">
            <?php if($businessesdata['Business']['totalReviews']>0) {?>
			<?php echo 'Overall feedback rating out of '.$businessesdata['Business']['totalReviews']?>
			<?php }?>              
			  </div>
			  </div>
                 
          </div>
		
		
			<?php 
				if(isset($design) && !empty($design['AgencysiteSetting']['sitebackgroundcolor']))
					{
						$headcolor = $design['AgencysiteSetting']['siteheadcolor'];  
				?>

				<!-- BEGIN HEADER -->

					<div class="dashboard-stat" style="background-color:#<?php print ("$headcolor"); ?>">

				<?php } else { ?>
					
					<div class="dashboard-stat blue">
					  
				<?php } ?>
            <div class="visual">
              <i class="fa fa-thumbs-up"></i>
            </div>
            
              <div class="rateavg">
                <?php if($online_average_rating[0]['AverageRating']>0){?>
            <span><?php echo number_format((float)$online_average_rating[0]['AverageRating'], 2, '.', '');?></span>
            <?php } else {?>
               <span><?php echo number_format((float)0, 2, '.', '');?></span>
            <?php }?>
              </div>
              <div class="details dside">
              
              <div class="desc">
                 Your Online Rating
              </div>
              <div class="more">
            Overall online rating <?php echo number_format((float)$online_average_rating[0]['AverageRating'], 2, '.', '');?><br>out of <?php echo number_format((float)$online_average_rating[0]['totalcount']);?>             
			  </div>
			  </div>
                 
          </div>
		
		
		</div>
            

