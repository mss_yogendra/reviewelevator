<script type="text/javascript">
$(document).ready(function(){
 
    $('#BusinessAddForm').validate({
          onfocusout: function (element) {
             $(element).valid();
            },
            rules:
            {
                "data[User][email]":
                {
                    required:true,
                    email:true,
                    remote:ajax_url+'admin/validUserEmail'
                },
                
                "data[User][password]":
                {
                    required:true,
                    minlength: 8
                },
               
                "data[User][firstname]":
                {
                    required:true,
                    accept: "[a-zA-Z]+",
                    maxlength:20
                },
                "data[User][lastname]":
                {
                    required:true,
                    accept: "[a-zA-Z]+",
                    maxlength:20
                },
                "data[BusinessEmployee][agency_id]":
                {
                    required:true,
                  
                },
	"data[User][cpassword]":
                {
                    required:true,
                    equalTo:'#pwd'
                },
                "data[BusinessEmployee][business_id]":
                {
                   required:true,
                  
                }
            },
            messages:
            {
                "data[User][email]":
                {
                    required:'Please enter email.',
                    email:'Please enter valid email.',
                    remote:'Email address already exists.'
                },
                "data[User][password]":
                {
                    required:"This field is required.",
                    minlength: 'Password should be atleast 8 characters long.'
                },
               
                "data[User][firstname]":
                {
                    required:"This field is required.",
                     accept :"Please enter only characters "
                },
                "data[User][lastname]":
                {
                    required:"This field is required.",
                   accept :"Please enter only characters "
                },
                "data[BusinessEmployee][agency_id]":
                {
                   required:"This field is required."
                  
                },
                "data[BusinessEmployee][business_id]":
                {
                   required:"This field is required."
                  
                }
            }
        
        
        });
        
        });
</script>
<?php echo $this->element('nav_admin')?>
<!-- BEGIN CONTENT -->
<?php echo $this->Session->flash(); ?>
<div class="page-content-wrapper">
	<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		   <?php echo $this->Session->flash(); ?>  
		  <h3 class="page-title">
		  ReForce Administration - Add Employee
		  </h3>
		  <div class="page-bar">
			<ul class="page-breadcrumb">
			  <li>
				<i class="fa fa-home"></i>
				<a href="<?php echo HTTP_ROOT?>">Administration</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
				<a href="#">Add Employee</a>
			  </li>
			</ul>
			<div class="page-toolbar">
			  
			</div>
		  </div>
		  <!-- END PAGE HEADER-->
        
			
			<div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-sm-12">
				<?php echo $this->Session->flash(); ?>  
				
				<!----------------------------------->
				<!----------------------------------->
					  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
				      <!--   >>  BEGIN PORTLET  << -->
					  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					  <div class="portlet box reforce-red">
					  
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>Add Employee
						  </div>
						  <div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->
						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
							
								<div class="col-sm-12">
									<span><a class="go-back btn btn-primary" href="<?php echo HTTP_ROOT?>admin/employees">Go Back</a></span>
								</div>

											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
									<div class="">
									
								 <form class="form-horizontal"  accept-charset="utf-8" method="post" id="BusinessAddForm" action="<?php echo HTTP_ROOT?>admin/addemp">

   
									<div class="form-group">
									  <label class="control-label col-sm-4" >First Name:</label>
									  <div class="col-sm-4">
										<input type="text" class="form-control form-back" placeholder="Enter First Name" name="data[User][firstname]">
									  </div>
									</div>


									<!-- <div class="form-group">
									  <label class="control-label col-sm-4">Last Login:</label>
									  <div class="col-sm-8">
										<input type="text" class="form-control form-back" placeholder="Enter Last Login" name="data[User][lastlogin]">
									  </div>
									</div> -->


									<div class="form-group">
									  <label class="control-label col-sm-4" > Last Name:</label>
									  <div class="col-sm-4">
										<input type="text" name="data[User][lastname]" placeholder="Enter Last Name" class="form-control form-back">
									  </div>
									</div>

									<div class="form-group">
									  <label class="control-label col-sm-4" for="email"> Email Id:</label>
									  <div class="col-sm-4">
										<input type="email" id="UserEmail" name="data[User][email]" placeholder="Enter Email Id" class="form-control form-back">
									  </div>
									</div>

									 <div class="form-group">
									  <label class="control-label col-sm-4" for="email">Password:</label>
									  <div class="col-sm-4">
										<input type="password" class="field text full required form-control form-back" id="pwd"  placeholder="Enter Password" name="data[User][password]" required="required">
									  </div>
									</div>

								   <div class="form-group">
									  <label class="control-label col-sm-4" for="email">Confirm Password:</label>
									  <div class="col-sm-4">
										<input type="password" class="field text full required form-control form-back" id="UserPassword" placeholder="Enter Confirm Password" name="data[User][cpassword]">
									  </div>
									</div>


									<div class="form-group">
										 <label class="control-label col-sm-4">Select Agency:</label> 
										 <div class="col-sm-4">
										<select class="form-selected form-control" required="required" id="find_bus" name="data[BusinessEmployee][agency_id]">
											  <option value=""><?php echo "Select Agency"?></option>
											  <?php foreach ($agnc as $key => $value) { ?>
												   <option value="<?php echo $value['User']['id']?>"><?php echo $value['User']['agencyname']?></option>
											  <?php }?>
											  
											   
									  </select>
											</div>
									</div>
									
									 <div class="form-group">
									  <label class="control-label col-sm-4" for="email">Business Name:</label>
									  <div class="col-sm-4">
										 <select class="form-selected form-control" required="required" id="find_emp" name="data[BusinessEmployee][business_id]">
											<option value="">Select business name</option>
										 </select></div>
									</div>
								  
									<div class="form-group">
									  <label class="control-label col-sm-4" >Status:</label>
									  <div class="col-sm-4">
										<input type="checkbox" name="data[User][status]">
									  </div>
									</div>



								  <div class="form-group">
									<label class="control-label col-sm-4" for="email">&nbsp;</label>  
									<div class="col-sm-4 submitting"> 
										<input type="submit" class="submit btn btn-primary" value="Submit">
									 </div> 
									</div>


								 
								</form>
									
									</div>
									<!-- MAIN END -->
								</div>
							</div>
							
						</div>
						<!-- END PORTLET BODY -->
						
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					<!--   >>   END PORTLET  <<  -->
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
			  <!----------------------------------->
			  <!----------------------------------->
		
				</div>
			</div> <!-- END LAYOUT ROW -->	
		
	</div>
</div>	

<script>
 $("#find_bus").change(function(){
      $.ajax({                   
                url: ajax_url+'admin/findBusiness',
                cache: false,
                type: 'POST',
                data: {'id':$(this).val()},
                dataType: 'json',
                success: function (bus) {
                   var options = '';
                  options = '<option value="0">Select Business</option>';
                  $.each(bus.html, function(index, bus) {
              options += '<option value="' + index + '">' + bus + '</option>';
          });
         // options1 = '<option value="0">Select City</option>';
          $('#find_emp').html(options);
          //$('#find_city').html(options1);
                }
            });
            return false;
    });
</script>
