<script type="text/javascript">
$(document).ready(function(){
    $('#newpartner').validate({
          onfocusout: function (element) {
             $(element).valid();
            },
            rules:
            {
                
                "data[User][firstname]":
                {
                     required:true,
                     accept: "[a-zA-Z]+",
		     maxlength:20
                },
                "data[User][lastname]":
                {
                     required:true,
                     accept: "[a-zA-Z]+",
		     maxlength:20
                },
                "data[User][email]":
                {
                    required:true,
                    email:true,
                   remote:ajax_url+'admin/validUserEmail'
                },
                
                "data[User][password]":
                {
                    required:true,
                    minlength: 8
                },
                "data[User][cpassword]":
                {
                    required:true,
                    equalTo:'#pwd'
                },
                 "data[agency][agencyname]":
                {
                  required:true
                },
                
            },
            messages:
            {
                "data[User][firstname]":
                {
                    required:"Please Enter The First Name",
                     accept :"Please enter only characters "
                },
                 "data[User][email]":
                {
                    required:'Please enter email.',
                    email:'Please enter valid email.',
                    remote:'Email address already exists.'
                },
                "data[User][lastname]":
                {
                     required:"Please Enter The Last Name",
                     accept :"Please enter only characters "
                },
               "data[agency][agencyname]":
                {
                  required:"Please Enter The Agency Name"
                },
                
                
            }
        
        
        });
        
    
    }); 
</script> 
<?php echo $this->element('nav_admin')?>
<!-- BEGIN CONTENT -->
<?php echo $this->Session->flash(); ?>
<div class="page-content-wrapper">
	<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		   <?php echo $this->Session->flash(); ?>  
		  <h3 class="page-title">
		  ReForce Administration - Add Agency
		  </h3>
		  <div class="page-bar">
			<ul class="page-breadcrumb">
			  <li>
				<i class="fa fa-home"></i>
				<a href="<?php echo HTTP_ROOT?>">Administration</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
				<a href="#">Add Agency</a>
			  </li>
			</ul>
			<div class="page-toolbar">
			  
			</div>
		  </div>
		  <!-- END PAGE HEADER-->
        
			
			<div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-sm-12">
				<?php echo $this->Session->flash(); ?>  
				
				<!----------------------------------->
				<!----------------------------------->
					  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
				      <!--   >>  BEGIN PORTLET  << -->
					  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					  <div class="portlet box reforce-red">
					  
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>Add Agency
						  </div>
						  <div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->
						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
							
								<div class="col-sm-12">
									<span><a class="go-back btn btn-primary" href="<?php echo HTTP_ROOT?>admin/partner">Go Back</a></span>
								</div>

											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
									<div class="">
									
								 <form class="form-horizontal"  accept-charset="utf-8" method="post" id="newpartner" action="<?php echo HTTP_ROOT?>admin/addAgency/<?= base64_encode($partners['Partner']['id'])?>">

   
								   <div class="form-group">
									  <label class="control-label col-sm-4" >First Name:</label>
									  <div class="col-sm-4">
										<input type="text" class="form-control form-back" placeholder="Enter First Name" name="data[User][firstname]" value="<?php echo @$data['User']['firstname']?>">
										<?php if(isset($errors['User']['firstname'])){?>
											<label class="error" generated="true" for="data[User][firstname]">
												<?php echo $errors['User']['firstname'];?>
											</label>
										<?php }?>
									  </div>
									</div>

									<div class="form-group">
									  <label class="control-label col-sm-4" >Last Name:</label>
									  <div class="col-sm-4">
										<input type="text" class="form-control form-back" placeholder="Enter Last Name" name="data[User][lastname]" value="<?php echo @$data['User']['lastname']?>">
										<?php if(isset($errors['User']['lastname'])){?>
											<label class="error" generated="true" for="data[User][lastname]">
												<?php echo $errors['User']['lastname'];?>
											</label>
										<?php }?>
									  </div>
									</div>

								 <div class="form-group">
									  <label class="control-label col-sm-4" >Agency Name:</label>
									  <div class="col-sm-4">
										<input type="text" class="form-control form-back" placeholder="Enter Agency Name" name="data[AgencysiteSetting][agencyname]" value="<?php echo @$data['agency']['agencyname']?>">
										<?php if(isset($errors['agency']['agencyname'])){?>
											<label class="error" generated="true" for="data[agency][agencyname]">
												<?php echo $errors['agency']['agencyname'];?>
											</label>
										<?php }?>
									  </div>
									</div>

									 <div class="form-group">
									  <label class="control-label col-sm-4" >Email:</label>
									  <div class="col-sm-4">
										<input type="email" name="data[User][email]" placeholder="Enter email" class="form-control form-back" value="<?php echo @$data['User']['email'];?>">
										<?php if(isset($errors['User']['email'])){?>
											<label class="error" generated="true" for="data[User][email]">
												<?php echo $errors['User']['email'];?>
											</label>
										<?php }?>
									  </div>
									</div>

									<div class="form-group">
									  <label class="control-label col-sm-4" for="email">Password:</label>
									  <div class="col-sm-4">
										<input type="password" class="field text full required form-control form-back" id="pwd"  placeholder="Enter Password" name="data[User][password]">
										<?php if(isset($errors['User']['password'])){?>
											<label class="error" generated="true" for="data[User][password]">
												<?php echo $errors['User']['password'];?>
											</label>
										<?php }?>
									  </div>
									</div>

									 <div class="form-group">
									  <label class="control-label col-sm-4" for="email">Confirm Password:</label>
									  <div class="col-sm-4">
										<input type="password" class="field text full required form-control form-back" id="UserPassword" placeholder="Enter Confirm Password" name="data[User][cpassword]">
										<?php if(isset($errors['User']['cpassword'])){?>
											<label class="error" generated="true" for="data[User][cpassword]">
												<?php echo $errors['User']['cpassword'];?>
											</label>
										<?php }?>
									  </div>
									</div>

								  
									<div class="form-group">
									  <label class="control-label col-sm-4" >Status:</label>
									  <div class="col-sm-4">
									   <input type="checkbox" name="data[User][status]" value="1">
									  </div>
									</div>



								  <div class="form-group">
									<label class="control-label col-sm-4" for="email">&nbsp;</label>  
									<div class="col-sm-8 submitting"> 
										<input type="submit" class="submit btn btn-primary" value="Submit">
									</div> 
								  </div>


								 
								</form>
									
									</div>
									<!-- MAIN END -->
								</div>
							</div>
							
						</div>
						<!-- END PORTLET BODY -->
						
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					<!--   >>   END PORTLET  <<  -->
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
			  <!----------------------------------->
			  <!----------------------------------->
		
				</div>
			</div> <!-- END LAYOUT ROW -->	
		
	</div>
</div>	

