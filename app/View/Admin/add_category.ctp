<script type="text/javascript">
$(document).ready(function(){
    $('#BusinessAddForm').validate({
          onfocusout: function (element) {
             $(element).valid();
            },
            rules:
            {
                
                "data[BusinessCategory][name]":
                {
                    required:true
                },
               
                
            },
            messages:
            {
                "data[BusinessCategory][name]":
                {
                    required:"This field is required."
                },
               
            }
        
        
        });
        
    
    });
</script>
<?php echo $this->element('nav_admin')?>
<!-- BEGIN CONTENT -->
<?php echo $this->Session->flash(); ?>
<div class="page-content-wrapper">
	<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		   <?php echo $this->Session->flash(); ?>  
		  <h3 class="page-title">
		  ReForce Administration - Add Category
		  </h3>
		  <div class="page-bar">
			<ul class="page-breadcrumb">
			  <li>
				<i class="fa fa-home"></i>
				<a href="<?php echo HTTP_ROOT?>">Administration</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
				<a href="#">Add Category</a>
			  </li>
			</ul>
			<div class="page-toolbar">
			  
			</div>
		  </div>
		  <!-- END PAGE HEADER-->
        
			
			<div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-sm-12">
				<?php echo $this->Session->flash(); ?>  
				
				<!----------------------------------->
				<!----------------------------------->
					  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
				      <!--   >>  BEGIN PORTLET  << -->
					  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					  <div class="portlet box reforce-red">
					  
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>Add Category
						  </div>
						  <div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->
						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
							
								<div class="col-sm-12">
									<span><a class="go-back btn btn-primary" href="<?php echo HTTP_ROOT?>admin/category">Go Back</a></span>
								</div>

											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
									<div class="">
									
								 <form class="form-horizontal"  accept-charset="utf-8" method="post" id="BusinessAddForm" action="<?php echo HTTP_ROOT?>admin/add_category">

   
								<div class="form-group">
								  <label class="control-label col-sm-4" >Category Name:</label>
								  <div class="col-sm-4">
									<input type="text" class="form-control form-back" placeholder="Enter Category Name" name="data[BusinessCategory][name]">
								  </div>
								</div>

								<div class="form-group">
								  <label class="control-label col-sm-4" >Status:</label>
								  <div class="col-sm-4">
								   <input type="checkbox" name="data[User][status]" value="1">
								  </div>
								</div>



							  <div class="form-group">
								<label class="control-label col-sm-4" for="email">&nbsp;</label>  
								<div class="col-sm-4 submitting"> 
									<input type="submit" class="submit btn btn-primary" value="Submit">
								 </div> 
								</div>


							 
							</form>
									
									</div>
									<!-- MAIN END -->
								</div>
							</div>
							
						</div>
						<!-- END PORTLET BODY -->
						
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					<!--   >>   END PORTLET  <<  -->
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
			  <!----------------------------------->
			  <!----------------------------------->
		
				</div>
			</div> <!-- END LAYOUT ROW -->	
		
	</div>
</div>	
