<script>
$(document).ready(function(){
 
    $('#addpartner').validate({
          onfocusout: function (element) {
             $(element).valid();
            },
            rules:
            {
                "data[Partner][user_id]":
                {
                    required:true,
                  
                }
            },
            messages:
            {
                "data[Partner][user_id]":
                {
                    required:'This field is required.',
                   
                   
                }
               
            }
        
        
        });
        
        });
</script>
<?php echo $this->element('nav_admin')?>
<!-- BEGIN CONTENT -->
<?php echo $this->Session->flash(); ?>
<div class="page-content-wrapper">
	<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		   <?php echo $this->Session->flash(); ?>  
		  <h3 class="page-title">
		  ReForce Administration - Add New Partner
		  </h3>
		  <div class="page-bar">
			<ul class="page-breadcrumb">
			  <li>
				<i class="fa fa-home"></i>
				<a href="<?php echo HTTP_ROOT?>">Administration</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
				<a href="#">Add Partner</a>
			  </li>
			</ul>
			<div class="page-toolbar">
			  
			</div>
		  </div>
		  <!-- END PAGE HEADER-->
			
			<div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-sm-12">
				<?php echo $this->Session->flash(); ?>  
				      <!--   >>  BEGIN PORTLET  << -->
					  <div class="portlet box reforce-red">
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>Add Partner
						  </div>
						  <div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->
						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
							
								<div class="col-sm-12">
									<span><a class="go-back btn btn-primary" href="<?php echo HTTP_ROOT?>admin/partner">Go Back</a></span>
								</div>

											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
									<div class="">
									
								<form class="form-horizontal"  accept-charset="utf-8" method="post" id="addpartner" action="<?php echo HTTP_ROOT?>admin/addpartner">
									<div class="form-group">
										 <label class="control-label col-sm-4">Select Agency name to assign as a Partner:</label> 
										 <div class="col-sm-4">
										<select class="form-selected form-control" id="find_bus" name="data[Partner][user_id]">
											  <option value=""><?php echo "Select Agency"?></option>
											  <?php foreach ($agnc as $key => $value) { ?>
												   <option value="<?php echo $value['User']['id']?>"><?php echo $value['User']['agencyname']?></option>
											  <?php }?>
									  </select></br>
									   <div class="form-group">
									 
									<div class="col-sm-4 submitting"> 
										<input type="submit" class="submit btn btn-primary" value="Submit">
										
									 </div> 
									</div>
									<b>OR</b></br></br>
									  <a type="submit" class="submit btn btn-primary" href="<?= HTTP_ROOT?>admin/newPartner">Create New Account</a>
									  <?php if(isset($errors['Partner']['user_id'])){?>
											<label class="error" generated="true" for="data[Partner][user_id]">
												<?php echo $errors['Partner']['user_id'];?>
											</label>
										<?php }?>
											</div>
									</div>
								
								 
								</form>
									
									</div>
									<!-- MAIN END -->
								</div>
							</div>
							
						</div>
						<!-- END PORTLET BODY -->
						
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					<!--   >>   END PORTLET  <<  -->
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
			  <!----------------------------------->
			  <!----------------------------------->
		
				</div>
			</div> <!-- END LAYOUT ROW -->	
		
	</div>
</div>	

<script>
 /*$("#find_bus").change(function(){
      $.ajax({                   
                url: ajax_url+'admin/findBusiness',
                cache: false,
                type: 'POST',
                data: {'id':$(this).val()},
                dataType: 'json',
                success: function (bus) {
                   var options = '';
                  options = '<option value="">Select Business Name</option>';
                  $.each(bus.html, function(index, bus) {
              options += '<option value="' + index + '">' + bus + '</option>';
          });
         // options1 = '<option value="0">Select City</option>';
          $('#find_emp').html(options);
          //$('#find_city').html(options1);
                }
            });
            return false;
    });*/
</script>
