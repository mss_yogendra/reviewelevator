<script type="text/javascript">
$(document).ready(function(){
    $('#BusinessAddForm12').validate({
          onfocusout: function (element) {
             $(element).valid();
            },
            rules:
            {
                "data[Business][businessname]":
                {
                    required:true
                },
                "data[Business][agency_id]":
                {
                    required:true
                },
                "data[Business][business_category_id]":
                {
                    required:true
                },
                "data[User][email]":
                {
                    required:true,
                    email:true,
                     remote:ajax_url+'admin/validUserEmail'
                },
                
                "data[User][password]":
                {
                    required:true,
                    minlength: 8
                },
                "data[User][cpassword]":
                {
                    required:true,
                    equalTo:'#pwd'
                }
                
            },
            messages:
            {
                "data[Business][businessname]":
                {
                    required:"This field is required."
                },
                "data[Business][agency_id]":
                {
                    required:"This field is required."
                },
                "data[Business][business_category_id]":
                {
                    required:"This field is required."
                },
                "data[User][email]":
                {
                    required:'Please enter email.',
                    email:'Please enter valid email.',
                    remote:'Email address already exists.'
                },
                
            }
        
        
        });
        
    
    });
</script>
<?php echo $this->element('nav_admin')?>
<!-- BEGIN CONTENT -->
<?php echo $this->Session->flash(); ?>
<div class="page-content-wrapper">
	<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		   <?php echo $this->Session->flash(); ?>  
		  <h3 class="page-title">
		  ReForce Administration - Add Business
		  </h3>
		  <div class="page-bar">
			<ul class="page-breadcrumb">
			  <li>
				<i class="fa fa-home"></i>
				<a href="<?php echo HTTP_ROOT?>">Administration</a>
				<i class="fa fa-angle-right"></i>
			  </li>
			  <li>
				<a href="#">Add Business</a>
			  </li>
			</ul>
			<div class="page-toolbar">
			  
			</div>
		  </div>
		  <!-- END PAGE HEADER-->
        
			
			<div class="row"> <!-- ROW & COL FOR LAYOUT -->
				<div class="col-sm-12">
				<?php echo $this->Session->flash(); ?>  
				
				<!----------------------------------->
				<!----------------------------------->
					  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
				      <!--   >>  BEGIN PORTLET  << -->
					  <!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					  <div class="portlet box reforce-red">
					  
						<!-- BEGIN PORTLET TITLE -->
						<div class="portlet-title">
						  <div class="caption">
							<i class="fa fa-user"></i>Add Business
						  </div>
						  <div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title="">
							</a>
							<!-- <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
							</a> -->
							<a href="" class="fullscreen" data-original-title="" title="">
							</a>
							<!-- <a href="javascript:;" class="reload" data-original-title="" title="">
							</a> -->
						  </div>
						</div>
						<!-- END TITLE -->
						
						<!-- BEGIN PORTLET BODY -->
						<div class="portlet-body grey">
						
						
						  <!-- TOOLBAR (PORT) -->
						  <div class="table-toolbar">
						  
							<div class="row">
							
								<div class="col-sm-12">
									<span><a class="go-back btn btn-primary" href="<?php echo HTTP_ROOT?>admin/agencyBusiness">Go Back</a></span>
								</div>

											
							</div>
						  </div>
						  <!-- TOOLBAR END (PORT) -->
						  
							<div class="row">
								<div class="col-md-12">
									<!-- PORTLET MAIN CONTENT -->
									<div class="">
									
								 <form class="form-horizontal"  accept-charset="utf-8" method="post" id="BusinessAddForm" action="<?php echo HTTP_ROOT?>admin/newbusiness/<?= base64_encode($partners['Partner']['id'])?>">

								<div class="form-group">
								 <label class="control-label col-sm-4" for="email">Agency Name:</label>
								 <div class="col-sm-4">
									<select class="form-selected form-control"  id="find_bus" name="data[Business][agency_id]">
									  <option value=""><?php echo "Select agency name"?></option>
										  <?php foreach($agency_list as $val){?>
											  <option value="<?php echo $val['User']['id']?>"><?php echo $val['User']['agencyname'];?></option>
										  <?php } ?>
									</select>
                                <?php if(isset($errors['Business']['agency_id'])){?>
											<label class="error" generated="true" for="data[Business][agency_id]">
												<?php echo $errors['Business']['agency_id'];?>
											</label>
										<?php }?>
								</div>
								</div>


								<div class="form-group">
								  <label class="control-label col-sm-4" >Business Name:</label>
								  <div class="col-sm-4">
									<input type="text" class="form-control form-back" placeholder="Enter Business Name" name="data[Business][businessname]" value="<?php echo @$data['User']['businessname'];?>">
									<?php if(isset($errors['Business']['businessname'])){?>
											<label class="error" generated="true" for="data[Business][businessname]">
												<?php echo $errors['Business']['businessname'];?>
											</label>
										<?php }?>
								  </div>
								</div>

								<div class="form-group">
								   <label class="control-label col-sm-4" for="email">Main Business Category:</label>
								   <div class="col-sm-4">
									 <select class="form-selected form-control"  id="BusinessBusinessCategoryId" name="data[Business][business_category_id]">
											<option value=""><?php echo "Select Bussiness Category"?></option>
												<?php foreach($businessCategories as $key=>$val){?>
													<option value="<?php echo $key?>"><?php echo $val?></option>
												<?php } ?>
									</select></br>
									<?php if(isset($errors['Business']['business_category_id'])){?>
											<label class="error" generated="true" for="data[Business][business_category_id]">
												<?php echo $errors['Business']['business_category_id'];?>
											</label>
										<?php }?>
								  </div>
								</div>


								<!--<div class="form-group">
								  <label class="control-label col-sm-4" >Contact Person:</label>
								  <div class="col-sm-8">
									<input type="text" name="data[User][firstname]" placeholder="Enter name" class="form-control form-back">
								  </div>
								</div> -->

								<div class="form-group">
								  <label class="control-label col-sm-4" >Email:</label>
								  <div class="col-sm-4">
									<input type="email" name="data[User][email]" placeholder="Enter email address" class="form-control form-back" value="<?php echo @$data['User']['email'];?>">
									<?php if(isset($errors['User']['email'])){?>
											<label class="error" generated="true" for="data[User][email]">
												<?php echo $errors['User']['email'];?>
											</label>
										<?php }?>
								  </div>
								</div>

								<div class="form-group">
								  <label class="control-label col-sm-4" >Password:</label>
								  <div class="col-sm-4">
									<input type="password" id="pwd" name="data[User][password]" placeholder="Enter password" class="form-control form-back" value="<?php echo @$data['User']['password'];?>">
									<?php if(isset($errors['User']['password'])){?>
											<label class="error" generated="true" for="data[User][password]">
												<?php echo $errors['User']['password'];?>
											</label>
										<?php }?>
								  </div>
								</div>

								<div class="form-group">
								  <label class="control-label col-sm-4" >Confirm Password:</label>
								  <div class="col-sm-4">
									<input type="password" id="UserPassword" name="data[User][cpassword]" placeholder="Enter password" class="form-control form-back" value="<?php echo @$data['User']['cpassword'];?>">
									<?php if(isset($errors['User']['cpassword'])){?>
											<label class="error" generated="true" for="data[User][cpassword]">
												<?php echo $errors['User']['cpassword'];?>
											</label>
										<?php }?>
								  </div>
								</div>

								<div class="form-group">
								  <label class="control-label col-sm-4" >Status:</label>
								  <div class="col-sm-4">
								   <input type="checkbox" name="data[User][status]" value="1">
								  </div>
								</div>


							  <div class="form-group">
								<label class="control-label col-sm-4" for="email">&nbsp;</label>  
								<div class="col-sm-4 submitting"> 
									<input type="submit" class="submit btn btn-primary" value="Submit">
								 </div> 
								</div>


							 
							</form>
									
									</div>
									<!-- MAIN END -->
								</div>
							</div>
							
						</div>
						<!-- END PORTLET BODY -->
						
					</div>
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
					<!--   >>   END PORTLET  <<  -->
					<!-- @@@ @@@ @@@ @@@ @@@ @@@ -->
			  <!----------------------------------->
			  <!----------------------------------->
		
				</div>
			</div> <!-- END LAYOUT ROW -->	
		
	</div>
</div>	
