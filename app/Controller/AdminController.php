<?php 
App::uses('AppController', 'Controller');

class AdminController extends AppController {
  	
 
public $components = array('Paginator');
function beforeFilter()
           {
                         parent::beforeFilter();
						$this->Auth->allow('postReview','thanksToCustomer','thanksToNegativeCustomer');   
             
           }
 
/*public function index()
 {


 	$userType=$this->Session->read('Auth.User.usertype');
 	if($userType == 'admin')
 	{
	 	$this->loadModel('User');
	 	$this->paginate= array('limit'=>'15','conditions'=>array('User.usertype'=>'reseller'),'order'=>array('User.id'=>'DESC'));
	 	$this->set('agency_data',$this->paginate('User'));
	 	if($this->request->is('post'))
          	{  $data = $this->data; 
                $this->loadModel('User');                   
                $search = $data['searchForm']['search'];
				$search = trim($search);
				$this->paginate = array(
					    'conditions' => array('User.usertype'=>'reseller',
					    	'OR'=>array('User.firstname LIKE' => '%'.$search.'%','User.lastname LIKE' => '%'.$search.'%')));	
				$this->set('agency_data',$this->paginate('User'));
                                $this->set('searchText',$search);
								   
		 }
	 	
	}
	else
	{
		$this->Session->setFlash('You are not authorised...!!!');
 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
	}
 }*/

  public function index( $id = null )
 {
 	$userType=$this->Session->read('Auth.User.usertype');
 	$this->loadModel('User');
 	if($userType == 'admin')
 	{
 		$id = base64_decode( $id );
 	/* start update by vipin for partner listing */
 		if( isset( $id ) && !empty( $id ))
 		{
 		  		 $this->loadModel('Partner');	
 		  		 $partnerData = $this->Partner->find('first',array('conditions'=>array('Partner.id'=>$id),'fields'=>array('Partner.user_id','Partner.agencyIds')));
				 $this->paginate = array(

					    'conditions' => array('OR'=>array('User.id' => $partnerData['Partner']['agencyIds'])));	
  				 $this->set('agency_data',$this->paginate('User'));

 		}
   /* End update by vipin for partner listing */
	 	else
 		{
	 	$this->paginate= array('limit'=>'15','conditions'=>array('User.usertype'=>'reseller'),'order'=>array('User.id'=>'DESC'));
	 	$this->set('agency_data',$this->paginate('User'));
	 	if($this->request->is('post'))
          	{  $data = $this->data; 
                $this->loadModel('User');                   
                $search = $data['searchForm']['search'];
				$search = trim($search);
				$this->paginate = array(
					    'conditions' => array('User.usertype'=>'reseller',
					    	'OR'=>array('User.firstname LIKE' => '%'.$search.'%','User.lastname LIKE' => '%'.$search.'%')));	
				
				$this->set('agency_data',$this->paginate('User'));
                                $this->set('searchText',$search);
								   
		 }

		}
	 	
	}
	else
	{
		$this->Session->setFlash('You are not authorised...!!!');
 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
	}
 }

  public function add(){
 	$this->loadModel('User');
 	$this->loadModel('AgencysiteSetting');
 	$userType=$this->Session->read('Auth.User.usertype'); 
 	if($userType == 'admin'){
	 	if(!empty($this->data)){
	 		$data = $this->data;
			$data['User']['usertype'] = 'reseller';
			
			if($data['User']['status'] ==''){
				$data['User']['status']=0;
			} 
			
 			if($this->User->save($data)){
 				$id = $this->User->getLastInsertId();
 				$info['AgencysiteSetting']['user_id'] = $id;
 				$info['AgencysiteSetting']['agencyname'] = $data['agency']['agencyname'];
 				 $info['AgencysiteSetting']['bussinesslimit']= $data['agency']['bussinesslimit'];
 				if($this->AgencysiteSetting->save($info))
 				{ 
 				$this->Session->setFlash('Agency added successfully.','success');
 				$this->redirect('/admin/index');
 			   }
 			}
		}
 	}else{
 		$this->Session->setFlash('You are not authorized user to access that location.','error');
 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
 	}
 }
/*public function updatestatusagency($id = null, $status = null){
	    $st=$status;
		$this->loadModel('User');
		$this->loadModel('Business');
		$this->loadModel('BusinessEmployee');
		$id= base64_decode($id);
		$agency['User']['id'] = $id;
		$agency['User']['status'] = $status;
		if($this->User->save($agency))
		{
            $agency_data = $this->User->find('first',array('conditions'=>array('User.id'=>$id),'recursive'=> -1)); 
            if(@$agency_data['User']['usertype'] == 'reseller' && @$agency_data['User']['status'] == $status)
            {
            	$conditions = array('Business.agency_id'=>@$agency_data['User']['id']);
            	$agency_business = $this->Business->find('all',array('contain'=>array('User'),'conditions'=>$conditions));
            	foreach (@$agency_business as $key => $value) 
            	{
            		$business['User']['id'] = @$value['Business']['user_Id'];
            		if($status==1){
            			if(isset($value['Business']['stage']) && $value['Business']['stage']){
            			$business['User']['status']=$value['Business']['stage'];
	            		}else{
	            			$business['User']['status']=0;
	            		}
            		}else{
            			$business['User']['status'] = $status;
            		}
            		if($this->User->save(@$business))
            		{
            			if($status==0){
            				$value['Business']['stage']=$value['User']['status'];
            				$this->Business->save($value);
            			}
            			$conditions = array('BusinessEmployee.business_id'=>$value['Business']['id']);
            			$business_employee = $this->BusinessEmployee->find('all',array('contain'=>array('User'),'conditions'=>$conditions));
            			foreach (@$business_employee as $key1 => $value1) 
            			{
            				
            			  	$employee['User']['id'] = @$value1['BusinessEmployee']['user_id'];
            			  	if($status==1){
		            			if(isset($value1['BusinessEmployee']['stage']) && $value1['BusinessEmployee']['stage']){
		            				$employee['User']['status']=$value1['BusinessEmployee']['stage'];
			            		}else{
			            			$employee['User']['status']=0;
			            		}
		            	    }else{
		            	    	$employee['User']['status'] = $status;
		            	    }
	            		    if($this->User->save(@$employee)){
		            		      	if($status==0){
			            				$value1['BusinessEmployee']['stage']=$value1['User']['status'];
			            				$this->BusinessEmployee->save($value1);
		            				}
	            		    }

            			}
            		}
        	    }

            }
             $response[]='success';
             $response[]=base64_encode($id);
             if($st==1){
             	$st=0;
             }else{
  				$st=1;           	
             }
             $response[]=$st;
             $response[]=$id;
             echo json_encode($response);die;
          
		}

	}*/


	public function updatestatusagency($id = null, $status = null){
	     
	    $st=$status;
		$this->loadModel('User');
		$this->loadModel('Business');
		$this->loadModel('BusinessEmployee');
		$this->loadModel('Partner');
		/* load this model for change status of multiloaction manager*/
		$this->loadModel('MultilocationManager');
		/* load model for change status of multiloaction manager*/
		$id = base64_decode($id);
       
        /* start updated by vipin date 08May2015 */

    /* status change for multilocation manager that having this agency as parent Agency Updated by vipin date 11-05-15 */
			
		
		$conditions = array('MultilocationManager.agency_id'=>$id);
		$agencyIds_multilocation = $this->MultilocationManager->find('all',array('conditions'=>$conditions,'fields' => array('MultilocationManager.user_id')));
		$agency['User']['id'] = $id;
		$agency['User']['status'] = $status;
		$this->User->save($agency);
		if( count( $agencyIds_multilocation ) > 0 )
			{
				foreach ($agencyIds_multilocation as $key1 => $value1) 
				{  
					$agencydatastatus = $this->User->find('first',array('conditions'=>array('User.id'=>@$value1['MultilocationManager']['user_id']),'fields'=>array('status','stage'),'contain'=>array(
						'MultilocationManager')));

				if($status==1){
		            			if(isset($agencydatastatus['User']['stage'])) {
		            				$agency1['User']['status']=$agencydatastatus['User']['stage'];
		            				$agency1['User']['stage'] = $agencydatastatus['User']['status'];
			            		}else{
			            			$agency1['User']['status']=0;
			            			$agency1['User']['stage']= @$agencydatastatus['User']['status'];
			            		}
		            	    }else{
		            	    	$agency1['User']['status'] = $status;
		            	    	$agency1['User']['stage'] = @$agencydatastatus['User']['status'];
		            	    } 
            $agency1['User']['id'] = @$value1['MultilocationManager']['user_id'];
            $this->User->save(@$agency1);
            }
             $response[]='success';
             $response[]=base64_encode($id);
             if($st==1){
             	$st=0;
             }else{
  				$st=1;           	
             }
             $response[]=$st;
             $response[]=$id;
             echo json_encode($response);die;
		}

			/*status change for multilocation manager that having this agency as parent Agency Updated by vipin date 11-05-15 */
		$parAgenIds = $this->Partner->find('all',array('fields'=>array('agencyIds','user_id')));
		foreach ($parAgenIds as $key => $value) {
            
            if(isset($value['Partner']['agencyIds']) && !empty($value['Partner']['agencyIds']))
            {	
			foreach ($value['Partner']['agencyIds'] as $k => $v) {
			 	
		 	    if($id == $v)
		 	    {
                   $partner_Id = $value['Partner']['user_id'];
                   break;
                } 	
		     }
		  }
		  
		 }
		 if( isset( $partner_Id ) )
		 {  
		 	$status1 = $this->User->find('first',array('conditions'=>array('User.id'=>$partner_Id),'fields'=>array('status'),'recursive'=> -1)); 
		 }
		 if( isset( $status1['User']['status'] ) && $status1['User']['status'] == 0 )
		 {
	 	 $response[]='agency';
	     echo json_encode($response);die;
		 }
			
		/* End updated by vipin date 08May2015 */
		$agency['User']['id'] = $id;
		$agency['User']['status'] = $status;
		if($this->User->save($agency))
		{
			$agency_data = $this->User->find('first',array('conditions'=>array('User.id'=>$id),'recursive'=> -1)); 
            if(@$agency_data['User']['usertype'] == 'reseller' && @$agency_data['User']['status'] == $status)
            {
            	$conditions = array('Business.agency_id'=>@$agency_data['User']['id']);
            	$agency_business = $this->Business->find('all',array('contain'=>array('User'),'conditions'=>$conditions));
            	foreach (@$agency_business as $key => $value) 
            	{
            		$business['User']['id'] = @$value['Business']['user_Id'];
            		if($status==1){
            			if(isset($value['Business']['stage']) && $value['Business']['stage']){
            			$business['User']['status']=$value['Business']['stage'];
	            		}else{
	            			$business['User']['status']=0;
	            		}
            		}else{
            			$business['User']['status'] = $status;
            		}
            		if($this->User->save(@$business))
            		{
            			if($status==0){
            				$value['Business']['stage']=$value['User']['status'];
            				$this->Business->save($value);
            			}
            			$conditions = array('BusinessEmployee.business_id'=>$value['Business']['id']);
            			$business_employee = $this->BusinessEmployee->find('all',array('contain'=>array('User'),'conditions'=>$conditions));
            			foreach (@$business_employee as $key1 => $value1) 
            			{
            				
            			  	$employee['User']['id'] = @$value1['BusinessEmployee']['user_id'];
            			  	if($status==1){
		            			if(isset($value1['BusinessEmployee']['stage']) && $value1['BusinessEmployee']['stage']){
		            				$employee['User']['status']=$value1['BusinessEmployee']['stage'];
			            		}else{
			            			$employee['User']['status']=0;
			            		}
		            	    }else{
		            	    	$employee['User']['status'] = $status;
		            	    }
	            		    if($this->User->save(@$employee)){
		            		      	if($status==0){
			            				$value1['BusinessEmployee']['stage']=$value1['User']['status'];
			            				$this->BusinessEmployee->save($value1);
		            				}
	            		    }

            			}
            		}
        	    }

            }
             $response[]='success';
             $response[]=base64_encode($id);
             if($st==1){
             	$st=0;
             }else{
  				$st=1;           	
             }
             $response[]=$st;
             $response[]=$id;
             echo json_encode($response);die;
          
		}
	

	}
	public function updatestatusbusiness($id = null, $status = null)
	{
		$st=$status;
		$this->loadModel('User');
		$this->loadModel('Business');
		$this->loadModel('BusinessEmployee');
		$id= base64_decode($id);
		$business['User']['id'] = $id;
		$business['User']['status'] = $status;
		$business_user_data = $this->Business->find('first',array('conditions'=>array('Business.user_Id'=>$id),'recursive'=> -1)); 
		$con = array('User.id'=>$business_user_data['Business']['agency_id']);
        $agencyStatus = $this->User->find('first',array('conditions'=>$con,'fields'=>array('User.status'),'recursive'=> -1));
		 if($agencyStatus['User']['status'] == 0)
		 {
		   /*$this->Session->setFlash('This Business Agency is Deactivated Please Active Business Agency First..');
		   $this->redirect($this->referer());	*/
		   $response[]='agency';
		    echo json_encode($response);die;
		 }
		if($this->User->save($business))
		{
			 $business_user_data = $this->Business->find('first',array('conditions'=>array('Business.user_Id'=>$id),'recursive'=> -1)); 
			 
             $conditions = array('BusinessEmployee.business_id'=>@$business_user_data['Business']['id']);
		     $business_employee = $this->BusinessEmployee->find('all',array('contain'=>array('User'),'conditions'=>$conditions));
		     foreach(@$business_employee as $key=>$value)
		     {
     	          $employee['User']['id'] = @$value['BusinessEmployee']['user_id'];
     	          if($status==1){
     	          	if(isset($value['BusinessEmployee']['stage']) && $value['BusinessEmployee']['stage']){
     	          		$employee['User']['status']= $value['BusinessEmployee']['stage'];
     	          	}else{
     	          		$employee['User']['status']= 0;
     	          	}
     	          }else{
     	          	$employee['User']['status'] = $status;
     	          }
    		      if($this->User->save(@$employee)){
    		      	 if($status==0){
    		      	 	$value['BusinessEmployee']['stage']=$value['User']['status'];
    		      	 	$this->BusinessEmployee->save($value);
    		      	 }
    		      }      	
		     }

		    $response[]='success';
            $response[]=base64_encode($id);
            if($st==1){
             	$st=0;
             }else{
  				$st=1;           	
             }
            $response[]=$st;
            $response[]=$id;
            echo json_encode($response);die;
		}	
	}
	public function updatestatusemployee($id = null, $status = null)
	{
		$st=$status;
		$this->loadModel('User');
		$this->loadModel('BusinessEmployee');
		$this->loadModel('Business');
		$id= base64_decode($id);
		$employee['User']['id'] = $id;
		$employee['User']['status'] = $status;
		$cond = array('BusinessEmployee.user_id'=>$id);
		$b_id = $this->BusinessEmployee->find('first',array('conditions'=>$cond,'fields'=>array('BusinessEmployee.business_id'),'recursive'=> -1));
	     $con = array('Business.id'=>$b_id['BusinessEmployee']['business_id']);
	     $business_user_id = $this->Business->find('first',array('conditions'=>$con,'fields'=>array('Business.user_Id','Business.agency_id'),'recursive' => -1));
	     $condi = array('User.id'=>$business_user_id['Business']['user_Id']);
	     $businessStatus = $this->User->find('first',array('conditions'=>$condi,'fields'=>array('User.status')));
	     if($businessStatus['User']['status'] == 0)
	     {
	      $response[]='business'	;
	      echo json_encode($response);die;	
	     /* $this->Session->setFlash('This Business is Deactivated Please Active Business First..');
		  $this->redirect($this->referer());	*/
	     }
         $condi = array('User.id'=>$business_user_id['Business']['agency_id']);
	     $businessStatus = $this->User->find('first',array('conditions'=>$condi,'fields'=>array('User.status')));
	    
        if(!empty($businessStatus) && $businessStatus['User']['status'] == 0)
	     {
	      $response[]='agency'	;
	      echo json_encode($response);die;		
	      /*$this->Session->setFlash('This Business Agency is Deactivated Please Active Agency First..');
		  $this->redirect($this->referer());	*/
	     }
       if($this->User->save($employee))
		{
			$response[]='success';
            $response[]=base64_encode($id);
            if($st==1){
             	$st=0;
             }else{
  				$st=1;           	
             }
            $response[]=$st;
            $response[]=$id;
            echo json_encode($response);die;
			  /*  $this->Session->setFlash('Status has been updated successfully.');
		    	$this->redirect($this->referer());*/
		}	

	}


  public function editAgency($id=NULL,$partner=null){ 
 	$this->loadModel('User');
 	$this->loadModel('AgencysiteSetting');
 	$partner=$partner;

 	$this->Session->write('partner',$partner);
 	$edit_agency=$this->User->find('first',array('fields'=>array('id','firstname','lastname','lastlogin','email','status','agencyname'),'conditions'=>array('User.id'=>base64_decode($id)),'recursive'=>-1));
    $this->set(compact('edit_agency'));
 	$userType=$this->Session->read('Auth.User.usertype');
 	if($userType == 'admin')
 	{

	 	if(!empty($this->data))
	 	{
	 		$data = $this->data;
	 	
	 		if(empty($data['User']['status']))
	 		{
				$data['User']['status'] = 0;
			}
	 		if($this->User->save($data))
	 		{
	 			$con = array('AgencysiteSetting.user_id'=>$data['User']['id']);
                $agency_site_id = $this->AgencysiteSetting->find('first',array('conditions'=>$con,'fields'=>array('id'),'recursive'=> -1));
                $info['AgencysiteSetting']['id'] = $agency_site_id['AgencysiteSetting']['id'];
 				$info['AgencysiteSetting']['agencyname'] = $data['agency']['agencyname'];
 				if($this->AgencysiteSetting->save($info))
 				{ 

 					$this->Session->setFlash('Agency updated successfully.','success');
 					if($partner=='partner' || $id=='partner'){

 						$this->redirect($this->Auth->redirect('/admin/partner'));
 					}else{
 						$this->redirect($this->Auth->redirect('/admin/index'));
 					}
	 				
	 				
 			   }
	 			
	 		}
	 	}
 	}
 	else
 	{
 		$this->Session->setFlash('You are not authorized user to access that location.','error');
 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
 	}
 }


 	function delete($model,$id){
		$this->loadModel($model);
		$this->$model->delete(base64_decode($id));
		$this->Session->setFlash('Record deleted successfully.','success');
		$this->redirect($this->referer());
	}

	
  

/* function agencyBusiness($id = NULL)
  {
      	$userType=$this->Session->read('Auth.User.usertype');
      	if($userType == 'admin')
 		{
	        if(empty($id))
	        {
	 		$this->loadModel('Business');
		 	$this->paginate= array('limit'=>'15','conditions'=>array('Business.agency_id >'=>0,'Business.is_deleted'=>0),'order'=>array('Business.id'=>'DESC'));
		 	$this->set('Agency_bus',$this->paginate('Business'));
		 	}
		 	if(!empty($id))
	 		{
	 		$id = base64_decode($id); 
	 		$this->loadModel('Business');
		 	$this->paginate= array('limit'=>'15','conditions'=>array('Business.agency_id'=>$id,'Business.is_deleted'=>0),'order'=>array('Business.id'=>'DESC'));
		 	$this->set('Agency_bus',$this->paginate('Business'));
		 	$this->set('agency_id',$id);
                        $this->set('valid',1);
		    }
	        if($this->request->is('post'))
	        {                       
                                    $this->loadModel('Business');
	        	                    $data = $this->data;
	        	                    if(isset($data['searchForm']['agency_id']) && !empty($data['searchForm']['agency_id']))
	        	                    {  
                                    $search = $data['searchForm']['search'];
									$search = trim($search);
									$this->paginate = array(
										    'conditions' => array(
										    'Business.businessname LIKE'=>'%'.$search.'%','Business.agency_id'=>$data['searchForm']['agency_id'],'Business.is_deleted'=>0));	
									$this->set('Agency_bus',$this->paginate('Business'));
                                                                        $this->set('valid',1);   
                                                                        $this->set('searchText',$search);   
	        	                    }
	        	                    else
	        	                    {	
									$search = $data['searchForm']['search'];
									$search = trim($search);
									$this->paginate = array(
										    'conditions' => array(
										    'Business.businessname LIKE'=>'%'.$search.'%','Business.agency_id >'=>0,'Business.is_deleted'=>0));	
									$this->set('Agency_bus',$this->paginate('Business'));
                                                                        $this->set('searchText',$search); 
								   }
		 	}
		}
		else
		{
			$this->Session->setFlash('You are not authorised...!!!');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
		}
	}		
*/
	/*Start:Updated by M*/
  function agencyBusiness($id = NULL,$partner_id= NULL)
  {     

      	$userType=$this->Session->read('Auth.User.usertype');
      	if($userType == 'admin')
 		{
	        if(empty($id))
	        {
	 		$this->loadModel('Business');
		 	$this->paginate= array('limit'=>'15','conditions'=>array('Business.agency_id >'=>0,'Business.is_deleted'=>0),'order'=>array('Business.id'=>'DESC'));
		 	$this->set('Agency_bus',$this->paginate('Business'));
		 	}
		 	
		    else if(!empty($id) && !empty($partner_id))
		    {
		    $id = base64_decode($partner_id); 
	 		$this->loadModel('Business');
	 		$this->loadModel('Partner');
	 		$agIds=$this->Partner->find('first',array('contain'=>false,'conditions'=>array('Partner.user_id'=>$id),'fields'=>array('Partner.agencyIds')));
		 	$this->paginate= array('limit'=>'15','conditions'=>array('Business.agency_id'=>$agIds['Partner']['agencyIds'],'Business.is_deleted'=>0),'order'=>array('Business.id'=>'DESC'));
		 	$this->set('Agency_bus',$this->paginate('Business'));
		 	$this->set('agency_id',$id);
            $this->set('valid',1);
		    }
		    else
	 		{
		 		$id = base64_decode($id); 
		 		$this->loadModel('Business');
			 	$this->paginate= array('limit'=>'15','conditions'=>array('Business.agency_id'=>$id,'Business.is_deleted'=>0),'order'=>array('Business.id'=>'DESC'));
			 	$this->set('Agency_bus',$this->paginate('Business'));
			 	$this->set('agency_id',$id);
	                        $this->set('valid',1);
		    }
		    
	        if($this->request->is('post'))
	        {                       
                                    $this->loadModel('Business');
	        	                    $data = $this->data;
	        	                    if(isset($data['searchForm']['agency_id']) && !empty($data['searchForm']['agency_id']))
	        	                    {  
                                    $search = $data['searchForm']['search'];
									$search = trim($search);
									$this->paginate = array(
										    'conditions' => array(
										    'Business.businessname LIKE'=>'%'.$search.'%','Business.agency_id'=>$data['searchForm']['agency_id'],'Business.is_deleted'=>0));	
									$this->set('Agency_bus',$this->paginate('Business'));
                                                                        $this->set('valid',1);   
                                                                        $this->set('searchText',$search);   
	        	                    }
	        	                    else
	        	                    {	
									$search = $data['searchForm']['search'];
									$search = trim($search);
									$this->paginate = array(
										    'conditions' => array(
										    'Business.businessname LIKE'=>'%'.$search.'%','Business.agency_id >'=>0,'Business.is_deleted'=>0));	
									$this->set('Agency_bus',$this->paginate('Business'));
                                                                        $this->set('searchText',$search); 
								   }
		 	}
		}
		else
		{
			$this->Session->setFlash('You are not authorised...!!!');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
		}
	}
	/*End*/
	function add_business(){
		$userType=$this->Session->read('Auth.User.usertype');
		if($userType == 'admin'){
			$this->loadModel('BusinessCategory');
			$this->loadModel('User');
			$businessCategories = $this->BusinessCategory->find('list',array('conditions'=>array('BusinessCategory.status'=>1),'fields'=>array('id','name'),'recursive'=>-1,'order'=>'name ASC'));
			$agency_list = $this->User->find('all',array('contain'=>array('MultilocationManager'),'conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>'','MultilocationManager.id'=>''),'order'=>'agencyname ASC'));
			//pr($agency_list`	);die;
			$this->set(compact('businessCategories','agency_list'));
			if(!empty($this->data)){
				$data= $this->data;
				$this->loadModel('User');
				$this->loadModel('Business');
				$data['User']['usertype'] = 'subscriber';
				if($data['User']['status'] ==''){
					$data['User']['status']=0;
				}
				
				if($this->User->save($data)){
					$id = $this->User->getLastInsertId();
					$data['Business']['user_Id'] = $id;
                                        $data['Business']['createdat']=Date('Y-m-d');
					if($this->Business->save($data)){
						$this->Session->setFlash('Business added successfully.','success');
	 					$this->redirect('/admin/agencyBusiness');
					}
				}

			}
		}else{
	 		$this->Session->setFlash('You are not authorized user to access that location.','error');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
 		}
	}

	/*function edit_business($id=NULL){
		$userType=$this->Session->read('Auth.User.usertype');
		if($userType == 'admin'){
			$this->loadModel('Business');
			$this->loadModel('BusinessCategory');
			$this->loadModel('User');
			$id = base64_decode($id); 
			$edit_bus = $this->Business->find('first',array('contain'=>array('User'),'conditions'=>array('Business.id'=>$id)));
			//$businessCategories = $this->BusinessCategory->find('list',array('fields'=>array('id','name'),'recursive'=>-1));
			$businessCategories = $this->BusinessCategory->find('list',array('conditions'=>array('BusinessCategory.status'=>1),'fields'=>array('id','name'),'recursive'=>-1,'order'=>'name ASC'));
			$agency_list = $this->User->find('all',array('conditions'=>array('User.usertype'=>'reseller'),'recursive'=>-1));
			$this->set(compact('edit_bus','businessCategories','agency_list'));

			if(!empty($this->data)){
				$data = $this->data;
				$this->loadModel('User');
				if($this->Business->save($data)){
					$data['User']['id']=$data['Business']['user_Id'];
					if(empty($data['User']['status'])){
						$data['User']['status'] = 0;
					}
					if($this->User->save($data)){
						$this->Session->setFlash('Business updated successfully.');
	 					$this->redirect('/admin/agencyBusiness');
					}
				}
			}
		}else{
	 		$this->Session->setFlash('You are not authorised...!!!');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
 		}
	}*/
function edit_business($id=NULL){
		$userType=$this->Session->read('Auth.User.usertype');
		if($userType == 'admin'){
			$this->loadModel('Business');
			$this->loadModel('BusinessCategory');
			$this->loadModel('User');
			$id = base64_decode($id); 
			$edit_bus = $this->Business->find('first',array('contain'=>array('User'),'conditions'=>array('Business.id'=>$id)));
			if($id){
				$agency=$this->User->findById($edit_bus['Business']['agency_id']);
				//pr($agency);die;
				if(isset($agency['MultilocationManager']['id']) && !empty($agency['MultilocationManager']['id'])){
					$businessname=$agency['User']['agencyname'];
				}else{
					$businessname='';
				}
			}
			$this->set(compact('businessname'));
			$businessCategories = $this->BusinessCategory->find('list',array('conditions'=>array('BusinessCategory.status'=>1),'fields'=>array('id','name'),'recursive'=>-1,'order'=>'name ASC'));
			$agency_list = $this->User->find('all',array('conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>''),'recursive'=>-1,'order'=>'agencyname ASC'));
			 
			$this->set(compact('edit_bus','businessCategories','agency_list'));

			if(!empty($this->data)){
				$data = $this->data;
				$edit_bus = $this->Business->find('first',array('contain'=>array('User'),'conditions'=>array('Business.id'=>$data['Business']['id'])));
				$agency=$this->User->findById($edit_bus['Business']['agency_id']);
				
				if(isset($agency['MultilocationManager']['id']) && !empty($agency['MultilocationManager']['id'])){
					$data['Business']['businessname']=$agency['User']['agencyname'];
				}
				$this->loadModel('User');
				if($this->Business->save($data)){
					$data['User']['id']=$data['Business']['user_Id'];
					if(empty($data['User']['status'])){
						$data['User']['status'] = 0;
					}
					if($this->User->save($data)){
						$this->Session->setFlash('Business updated successfully.','success');
	 					$this->redirect('/admin/agencyBusiness');
					}
				}
			}
		}else{
	 		$this->Session->setFlash('You are not authorized user to access that location.','error');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
 		}
	}

	function employee_list($id){
		$this->loadModel('BusinessEmployee');
		$emp_list =$this->BusinessEmployee->find('all',array('conditions'=>array('BusinessEmployee.business_id'=>base64_decode($id))));
		$this->paginate = array('limit'=>'15','conditions'=>array('BusinessEmployee.business_id'=>base64_decode($id)),'order'=>array('BusinessEmployee.id'=>'DESC'));
		$this->set('emp_list',$this->paginate('BusinessEmployee'));
	}


	function category(){
		$userType=$this->Session->read('Auth.User.usertype');
		if($userType == 'admin'){
			$this->loadModel('BusinessCategory');
			if(isset($this->request->data['searchForm']['search'])){
				if($this->request->is('post')){ 
					$data = $this->data;
					$search = $data['searchForm']['search'];
					$search = trim($search);
					$this->paginate = array(
							'order'=>array('BusinessCategory.name ASC'),
						    'conditions' => array(
						    'BusinessCategory.name LIKE'=>'%'.$search.'%'));	
				}
				$this->set('bus_cat',$this->paginate('BusinessCategory'));
                                $this->set('searchText',$search);
			}else{
					$this->paginate = array('limit'=>'15','order'=>array('BusinessCategory.name'=>'ASC'),'recursive'=>-1);
					$this->set('bus_cat',$this->paginate('BusinessCategory'));
			}


	 	}else{
	 		$this->Session->setFlash('You are not authorised...!!!');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
 		}	
	}
	function add_category(){
		$userType=$this->Session->read('Auth.User.usertype');
		if($userType == 'admin'){
			if(!empty($this->data)){
				$this->loadModel('BusinessCategory');
				if ($this->BusinessCategory->save($this->data)) {
					$this->Session->setFlash('Category added successfully.','success');
	 				$this->redirect($this->Auth->redirect('/admin/category'));	
				}
			}
		}else{
	 		$this->Session->setFlash('You are not authorised...!!!','error');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
 		}
	}

	function edit_category($id=NULL){
		$userType=$this->Session->read('Auth.User.usertype');
		if($userType == 'admin'){
			$this->loadModel('BusinessCategory');
			$edit_category = $this->BusinessCategory->find('first',array('conditions'=>array('BusinessCategory.id'=>base64_decode($id)),'recursive'=>-1));
			$this->set(compact('edit_category'));
			
			if(!empty($this->data)){
				$data = $this->data;
				if(empty($data['User']['status'])){
				$data['User']['status'] = 0;
				}
				if(empty($this->data['BusinessCategory']['status'])){
					$data['BusinessCategory']['status'] = 0;
				}
				
				if ($this->BusinessCategory->save($data)) {
					$this->Session->setFlash('Category updated successfully.','success');
	 				$this->redirect($this->Auth->redirect('/admin/category'));	
				}
			}
		}else{
	 		$this->Session->setFlash('You are not authorised...!!!','error');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
 		}
	}

	function cat_updatestatus($id,$status)
	{
		$st=$status;
		$this->loadModel('BusinessCategory');
		$id= base64_decode($id);
		$data['BusinessCategory']['id'] = $id;
		$data['BusinessCategory']['status'] = $status;
		if($this->BusinessCategory->save($data))
		{
			$response[]='success';
            $response[]=base64_encode($id);
            if($st==1){
             	$st=0;
             }else{
  				$st=1;           	
             }
            $response[]=$st;
             $response[]=$id;
            echo json_encode($response);die;
			/*$this->Session->setFlash('Status has been updated successfully.');
			$this->redirect($this->referer());*/
		}

	}

	function bus_cat_list($id){
		$this->loadModel('Business');
		$this->paginate = array('limit'=>'15','conditions'=>array('Business.business_category_id'=>base64_decode($id)),'order'=>array('Business.id'=>'DESC'));
		$this->set('bus_cat_list',$this->paginate('Business'));
	}

	/*function customer($id = null)
	{
		 $userType=$this->Session->read('Auth.User.usertype');
      	if($userType == 'admin')
 		{
	        if(empty($id))
	        {
	 		$this->loadModel('Customer');
		 	$this->paginate= array('limit'=>'15','order'=>array('Customer.id'=>'DESC'));//pr($this->paginate('Customer'));die;
		 	$this->set('Customer',$this->paginate('Customer'));
		 	}
		 	if($this->request->is('post'))
            		 {  	$data = $this->data; 
                		$this->loadModel('Customer');                   
               			 $search = $data['searchForm']['search'];
				$search = trim($search);
				$this->paginate = array(
					    'conditions' => array('OR'=>array('Customer.firstname LIKE' => "%$search%",'Customer.lastname LIKE' => "%$search%",'Customer.email LIKE' => "%$search%")));	
				$this->set('Customer',$this->paginate('Customer'));
                                 $this->set('searchText',$search);
								   
		 	}
		}
		else
		{
			$this->Session->setFlash('You are not authorised...!!!');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
		}

	}*/
function customer($id = null)
	{
		 $userType=$this->Session->read('Auth.User.usertype');
      	if($userType == 'admin')
 		{
	        if(empty($id))
	        {
	 		$this->loadModel('Customer');
		 	$this->paginate= array('conditions'=>array('Customer.is_delete'=>0),'limit'=>'15','order'=>array('Customer.id'=>'DESC')); 
		 	$this->set('Customer',$this->paginate('Customer'));
		 	}
		 	if($this->request->is('post'))
            		 {  	$data = $this->data; 
                		$this->loadModel('Customer');                   
               			 $search = $data['searchForm']['search'];
				$search = trim($search);
				$this->paginate = array(
					    'conditions' => array('Customer.is_delete'=>0,'OR'=>array('Customer.firstname LIKE' => "%$search%",'Customer.lastname LIKE' => "%$search%",'Customer.email LIKE' => "%$search%")));	
				$this->set('Customer',$this->paginate('Customer'));
                                 $this->set('searchText',$search);
								   
		 	}
		}
		else
		{
			$this->Session->setFlash('You are not authorised...!!!');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
		}

	}

	
function add_customer()
{
$userType=$this->Session->read('Auth.User.usertype');
	if($userType == 'admin')
	{
		$this->loadModel('Country');
		$this->loadModel('Customer');
		$this->loadModel('User');
		$this->loadModel('Business');
		$countries = $this->Business->Country->find('list',array('fields'=>array('id','country_name'),'order'=>array('country_name ASC')));
                    $us = $countries[1];
		unset($countries[1]);
		$countries[1] = $us;
		$agency_list = $this->User->find('all',array('conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>''),'recursive'=>-1,'order'=>'agencyname ASC'));
		   $this->set(compact('countries','agency_list'));
		if(!empty($this->data))
		{  
			$data = $this->data;
			$business_info = $this->Business->find('first',array('contain'=>array('User'),'conditions'=>array('Business.id'=>$data['Customer']['business_id']),'fields'=>array('User.email','User.id','Business.businessname','Business.id')));
			$business_name = $business_info['Business']['businessname'];
			$business_email = $business_info['User']['email'];
			$user_id = base64_encode($business_info['User']['id']);
           if((@$data['Customer']['preview'] == 1) && (@$data['Customer']['permission_to_email'] =='on'))
            {  
            $this->request->data['Customer']['permission_to_email'] = 1;	
        	$this->request->data['Customer']['status'] = 'InFeedbackSequence';
        	$this->request->data['Customer']['emailstatuscounter'] = 1;
            }
            else
            {
            $this->request->data['Customer']['status'] = 'NotInFeedbackSequence';
            }
            if($this->Customer->save($this->request->data['Customer']));
			{ 
		    $todaydate = date("Y-m-d");
            $customer_id = $this->Customer->getLastInsertId();
            if((@$this->request->data['Customer']['preview'] == 1) && (@$this->request->data['Customer']['permission_to_email'] ==1))
				{ 		
		            $email = $data['Customer']['email'];
					$name = $data['Customer']['firstname']. ' '. $data['Customer']['lastname'];
					$url=Router::url('/admin/postReview?id='.$user_id.'&customer_id='.base64_encode($customer_id), true);
					$eTemplate=$this->getEmailcontent($business_info['Business']['id'],1);
					$replace=array('$cusotmername'=>$name,'$customeremail'=>$email,'$business_name'=>$business_name,'$business_email'=>$business_email,'$reviewurl'=>$url);
					$sendername=@$eTemplate['sendername'];
					$sendemail=@$eTemplate['senderemail'];
					$content=$eTemplate['emailcontent'];	
					$subject=@$eTemplate['emailsubject'];
					$receiveremail=$email;
					if($this->_sendingEmail($sendername,$senderemail=0,$receiveremail,$subject,$content,$replace)):
						  $this->Customer->updateAll(array('Customer.cronssentemaildate'=>"'$todaydate'"), array('Customer.id' => $customer_id));
			        	  $this->Session->setFlash('Customer has been successfully Added and also Sent A initialy feedback sequence','success');
			        	  $this->redirect('/admin/customer');
					else:
						 $this->Session->setFlash('not healthy email id','error');
					endif;
				}
				else
				{
					$this->Customer->updateAll(array('Customer.cronssentemaildate'=>"'$todaydate'"), array('Customer.id' => $customer_id));	
					$this->Session->setFlash('Customer has been successfully Added','success');
					$this->redirect('/admin/customer');
                }
             }
		}

	}
   else
   {
	$this->Session->setFlash('You are not authorized user to access that location.','error');
	$this->redirect(array('controller'=>'dashboard','action'=>'index'));
	}
}
function edit_customer($id=NULL)
{
  $userType=$this->Session->read('Auth.User.usertype');
  if($userType == 'admin')
  {
	$this->loadModel('Customer');
	$this->loadModel('User');
	$this->loadModel('Country');
	$this->loadModel('BusinessEmployee');
	if($this->request->is('post'))
       {
       	        $data = $this->data;
       	        $business_info = $this->Business->find('first',array('contain'=>array('User'),'conditions'=>array('Business.id'=>$data['Customer']['business_id']),'fields'=>array('User.email','User.id','Business.businessname','Business.id')));
			    $business_name = $business_info['Business']['businessname'];
			    $business_email = $business_info['User']['email'];
			    $user_id = base64_encode($business_info['User']['id']);
                if((@$data['Customer']['preview'] == 1) && (@$data['Customer']['permission_to_email'] == 'on'))
	              {
                    $data['Customer']['status'] = 'InFeedbackSequence';
	            	$data['Customer']['emailstatuscounter'] = 1;
	            	$data['Customer']['permission_to_email'] = 1;
                   }
                   else if(!(isset($data['Customer']['preview'])) && (!isset($data['Customer']['permission_to_email'])))
	              {
                    
	            	$data['Customer']['status'] = 'InFeedbackSequence';
	            	$data['Customer']['emailstatuscounter'] = 1;
	            	$data['Customer']['permission_to_email'] = 1;
                   }
                   else
	                {
                     $data['Customer']['status'] = 'NotInFeedbackSequence';
       			 	}	

       	        if($this->Customer->save($data))
				{
					 $todaydate = date("Y-m-d");
                     $customer_id = $this->data['Customer']['id'];
                    if((@$data['Customer']['preview'] == 1) && (@$data['Customer']['permission_to_email'] == 1))
					{   
						$user_id = base64_encode($business_info['User']['id']);
						$email = $this->request->data['Customer']['email'];
						$name = $this->request->data['Customer']['firstname']. ' '. $this->request->data['Customer']['lastname'];
						$url=Router::url('/admin/postReview?id='.$user_id.'&customer_id='.base64_encode($customer_id), true);
						$eTemplate=$this->getEmailcontent($business_info['Business']['id'],1);
						$replace=array('$cusotmername'=>$name,'$customeremail'=>$email,'$business_name'=>$business_name,'$business_email'=>$business_email,'$reviewurl'=>$url);
						$sendername=@$eTemplate['sendername'];
						$sendemail=@$eTemplate['senderemail'];
						$content=$eTemplate['emailcontent'];	
						$subject=@$eTemplate['emailsubject'];
						$receiveremail=$email;
						if($this->_sendingEmail($sendername,$senderemail=0,$receiveremail,$subject,$content,$replace)):
					    $this->Customer->updateAll(array('Customer.cronssentemaildate'=>"'$todaydate'"), array('Customer.id' => $customer_id));
		        	    $this->Session->setFlash('Customer has been successfully Added and also Sent A initialy feedback sequence','success');
		        	    $this->redirect('/admin/customer');
						else:
					    $this->Session->setFlash('not saved email id','error');
						endif;         
                             
            	}
                else
                {
                  	$this->Session->setFlash('Customer has been Updated successfully','success');
					$this->Session->setFlash('Customer has been updated successfully.','success');
					$this->redirect('/admin/customer');
				}
		}
	}
else
	 {	
				$edit_cus = $this->Customer->find('first',array('conditions'=>array('Customer.id'=>base64_decode($id))));
				$agency_list = $this->User->find('all',array('conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>''),'recursive'=>-1,'order'=>'agencyname ASC'));
                $bus_list = $this->Business->find('all',array('contain'=>array('User'),'fields'=>array('id','agency_id','status','businessname','is_deleted','User.id','User.status'),'conditions'=>array('Business.is_deleted'=>0,'Business.status'=>1,'User.status'=>1,'Business.agency_id'=>$edit_cus['Business']['agency_id'])));
		        $emp_list = $this->BusinessEmployee->find('all',array('conditions'=>array('Business.status'=>1,'Business.is_deleted'=>0,'User.status'=>1,'BusinessEmployee.business_id'=>$edit_cus['Customer']['business_id'])));
			    $countries = $this->Country->find('list',array('fields'=>array('id','country_name'),'order'=>array('country_name ASC')));

		 		$this->loadModel('State');
				$states= $this->State->find('list',array('fields'=>array('id','stateName'),'order'=>array('stateName ASC')));
			    $this->set('states',$states);
			    $this->set(compact('edit_cus','agency_list','emp_list','bus_list','countries'));
        }
    }
	else
		{
			$this->Session->setFlash('You are not authorized user to access that location.','error');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
		}
}
function validatZip()
		{
			$zip = trim($_REQUEST['data']['Customer']['zip']);
			$this->autoRender = false;
			$rexSafety = "/^[0-9]|([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$/";
            if (preg_match($rexSafety, $zip)) {
 			   echo "true";die;
			} else {
    		echo "false";die;
			}
		}

	function ormBusiness(){
		$this->loadModel('Business');
		$userType=$this->Session->read('Auth.User.usertype');
		if($userType == 'admin'){
			$this->paginate= array('conditions'=>array('Business.agency_id'=>NULL),'limit'=>'15','contain'=>array('BusinessCategory','User'=>array('AgencysiteSetting')),'order'=>array('Business.id'=>'DESC'));
	 		//pr($this->paginate('Business'));die;
	 		$this->set('bus_data',$this->paginate('Business'));
		}else{
	 		$this->Session->setFlash('You are not authorised...!!!');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
 		}
	}

	public function employees($id = null)
	{ 
      	$userType=$this->Session->read('Auth.User.usertype');
      	if($userType == 'admin')
 		{
	        if(empty($id))
	        {
	 		$this->loadModel('BusinessEmployee');
		 	$this->paginate= array('limit'=>'15','order'=>array('BusinessEmployee.id'=>'DESC'));
		 	//pr($this->paginate('BusinessEmployee'));die;
		 	$this->set('employee',$this->paginate('BusinessEmployee'));
		 	}
		 	if(!empty($id))
	 		{
	 		$id = base64_decode($id); 
	 		$this->loadModel('BusinessEmployee');
		 	$this->paginate= array('limit'=>'15','conditions'=>array('BusinessEmployee.business_id'=>$id),'order'=>array('BusinessEmployee.id'=>'DESC'));
		 	$this->set('employee',$this->paginate('BusinessEmployee'));
		 	$this->set('business_id',$id);
		    }
	        if($this->request->is('post'))
	        {                       

	        	                    $data = $this->data;
	        	                    if(isset($data['searchForm']['business_id']) && !empty($data['searchForm']['business_id']))
	        	                    {
                                    $search = $data['searchForm']['search'];
									$search = trim($search);
									$this->paginate = array(
										    'conditions' => array(
										    'BusinessEmployee.emp_name LIKE'=>'%'.$search.'%','BusinessEmployee.id=>'<> NULL,'BusinessEmployee.business_id'=>$data['searchForm']['business_id']));	
									$this->set('employee',$this->paginate('BusinessEmployee'));
                                                                         $this->set('searchText',$search);  
	        	                    }
	        	                    else
	        	                    {	
									$search = $data['searchForm']['search'];
									$search = trim($search);
									$this->paginate = array(
										    'conditions' => array(
										    'BusinessEmployee.emp_name LIKE'=>'%'.$search.'%','BusinessEmployee.id=>'<> NULL));	
									$this->set('employee',$this->paginate('BusinessEmployee'));
                                                                         $this->set('searchText',$search); 
								   }
		 	}
		}
		else
		{
			$this->Session->setFlash('You are not authorised...!!!');
	 		$this->redirect(array('controller'=>'dashboard','action'=>'index'));
		}

	}
public function addemp()
	{
		$this->loadModel('User');
		$agency_list = $this->User->find('all',array('conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>''),'recursive'=>-1,'order'=>'agencyname ASC'));
		$this->set('agnc',$agency_list);
		
		if($this->request->is('post')){
			$this->request->data['User']['usertype']="employee";
			$this->request->data['User']['createdat']=date('Y-m-d H:i:s');
			
			if(isset($this->request->data['User']['status']) && $this->request->data['User']['status']=='on'){
				$this->request->data['User']['status']=1;
			}else{
				$this->request->data['User']['status']=0;
			}
			if($this->User->save($this->request->data['User'])){
				$this->request->data['BusinessEmployee']['user_id']=$this->User->getLastInsertId();
				$this->request->data['BusinessEmployee']['emp_name']=$this->request->data['User']['firstname'].' '.$this->request->data['User']['lastname'];
				$this->request->data['BusinessEmployee']['created_at']=date('Y-m-d H:i:s');
				$this->loadModel('BusinessEmployee');
				if($this->BusinessEmployee->save($this->request->data['BusinessEmployee'])){
					$this->Session->setFlash('Employee has been added successfully.','success');
	 				$this->redirect(array('controller'=>'admin','action'=>'employees'));
				}
			}else{
				$this->Session->setFlash('Unable to save data ! Please try again.','error');
	 			$this->redirect(array('controller'=>'admin','action'=>'employees'));
			}
			
		}
	}
	
	public function editEmployee($id = null) 
{
			if($id)
			{       
				    $this->loadModel('User');
					$id=base64_decode($id);
					$this->loadModel('BusinessEmployee');
					$emp=$this->BusinessEmployee->find('first',array('conditions'=>array('BusinessEmployee.id'=>$id)));
					$this->set('emp',$emp);
					$this->loadModel('Business');
					$buss=$this->Business->find('all',array('contain'=>array('User'),'conditions'=>array('Business.is_deleted'=>0,'User.status'=>1,'Business.agency_id'=>$emp['Business']['agency_id'])));
					$this->set('buss',$buss);
					$agency_list = $this->User->find('all',array('conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>''),'recursive'=>-1,'order'=>'agencyname ASC'));
					$this->set('agnc',$agency_list);


			}
			elseif($this->request->is('post'))
			{    
					$this->loadModel('BusinessEmployee');
					$this->loadModel('AgencysiteSetting');
					$this->loadModel('User');
					$emp=$this->BusinessEmployee->find('first',array('conditions'=>array('BusinessEmployee.id'=>$this->request->data['BusinessEmployee']['id'])));

					$this->set('emp',$emp);
					$this->loadModel('Business');
					$buss=$this->Business->find('all',array('contain'=>array('User'),'conditions'=>array('Business.is_deleted'=>0,'User.status'=>1),'fields'=>array('Business.id','Business.businessname')));
					$this->set('buss',$buss);
					
					if(isset($this->request->data['User']['status']) && $this->request->data['User']['status']=='on')
					{
						$this->request->data['User']['status']=1;
					}
					else
					{
						$this->request->data['User']['status']=0;
					}
					
					if($this->User->save($this->request->data['User']))
					{  
						$this->loadModel('BusinessEmployee');
						$this->request->data['BusinessEmployee']['emp_name']=$this->request->data['User']['firstname'].' '.$this->request->data['User']['lastname']; 
					   if($this->BusinessEmployee->save($this->request->data['BusinessEmployee']))
					   {
	                        if($this->Business->save($this->request->data['Business']))
	                        {
							$this->Session->setFlash('Employee has been updated successfully.','success');
		 					$this->redirect(array('controller'=>'admin','action'=>'employees'));
						    } 
					   }
					}
			}
			else
			{
				$this->Session->setFlash('Invalid Employee ! Please try again.','error');
		 		$this->redirect(array('controller'=>'admin','action'=>'employees'));
			}
}		
	

	

	function sites(){
		
		$userType=$this->Session->read('Auth.User.usertype');
		if($userType == 'admin'){
			$this->loadModel('SocialMedia');
			$this->paginate= array('limit'=>'15','order'=>array('SocialMedia.mediasitename ASC'));
	 		//echo"<pre>";pr($this->paginate('SocialMedia'));die;
	 		$this->set('bus_site',$this->paginate('SocialMedia'));
	 		if($this->request->is('post'))
          	{  
          		$data = $this->data; 
                $search = $data['searchForm']['search'];
				$search = trim($search);
				//pr($search);die;
				$this->paginate = array(
						'order'=>array('SocialMedia.mediasitename ASC'),
					    'conditions' => array('SocialMedia.mediasitename LIKE'=>'%'.$search.'%'));	
				$this->set('bus_site',$this->paginate('SocialMedia'));
                $this->set('searchText',$search);
								   
		 }
		}else{
	 		$this->Session->setFlash('You are not authorised...!!!');
	 		$this->redirect($this->referer());
 		}
	}

	function editsite($id=null){
		$id=base64_decode($id);
		$this->loadModel('SocialMedia');
		if($id){
			$site=$this->SocialMedia->find('first',array('conditions'=>array('SocialMedia.id'=>$id)));
			$this->set('site',$site);
		}elseif($this->request->is('post')){
			// pr($this->request->data);die;
			if(isset($this->request->data['SocialMedia']['status'])){
				$this->request->data['SocialMedia']['status']=1;
			}else{
				$this->request->data['SocialMedia']['status']=0;
			}
		if($this->SocialMedia->save($this->request->data)){
			$this->Session->setFlash('Site has been updated successfully.','success');
	 		$this->redirect(array('controller'=>'admin','action'=>'sites'));
		}	
		}else{
			$this->Session->setFlash('You are not authorised...!!!','error');
	 		$this->redirect($this->referer());
		}
	}

	function business_list($id){
		$this->loadModel('Business');
		$this->paginate = array('limit'=>'15','conditions'=>array('Business.user_Id'=>base64_decode($id)),'order'=>array('Business.id'=>'DESC'));
		$this->set('bus_list',$this->paginate('Business'));
	}

	function searchAgency(){
		if($this->request->is('post')){
			$searchValue = $this->request->data['searchForm']['search'];
			$searchValue = trim($searchValue);
			$this->loadModel('AgencysiteSetting');
			$this->paginate = array(
					    'conditions' => array(
					    'AgencysiteSetting.agencyname LIKE' => "%$searchValue%"));
			$this->set('agency_data',$this->paginate('AgencysiteSetting'));
			return $this -> render('agency');
		}else {
			$this->redirect( '/agency' );
		}
	}

	function findBusiness(){
		$this->autoRender=false;
        if ($this->request->is('Ajax'))
        {
        	$this->loadModel('Business');
        	$bus_id = $this->data['id'];
        	$data1 = $this->Business->find('all',array('contain'=>array('User'),'conditions'=>array('Business.agency_id'=>$bus_id,'Business.is_deleted'=>0,'User.status'=>1,'Business.agency_id >'=>0), 'fields'=>array('id','businessname'),'order'=>array('businessname ASC'))); 
        	
	        $data=array();
	        foreach ($data1 as $key => $value) {
	        $data[$value['Business']['id']]=$value['Business']['businessname'];
	        }
	        asort($data);
	        $this->set('bus', $data);
            echo json_encode(array('html' => $data));
        }
	}

	function multiBusiness($agn=null){
		
		$this->autoRender=false;
        if ($this->request->is('Ajax'))
        {
        	$this->loadModel('Business');
        	$bus_id = $this->data['id'];
        	$ids=$this->data['ids'];
        	$data1 = $this->Business->find('all',array('contain'=>array('User'),'conditions'=>array('Business.agency_id'=>$bus_id,'Business.is_deleted'=>0,'User.status'=>1,'Business.agency_id >'=>0), 'fields'=>array('id','businessname'),'order'=>array('businessname ASC'))); 
        	
	        $data=array();
	        foreach ($data1 as $key => $value) {
	        $data[$value['Business']['id']]=$value['Business']['businessname'];
	        }
	        asort($data);
	        $html="<option value=''>Select Business</option>";
	        foreach ($data as $key => $value) {
	        	if($agn==$bus_id){
	        		if(in_array($key, $ids)){
	        			$html.='<option value="'.$key.'"'.' '.'selected="selected" >'.$value.'</option>';
	        		}else{
	        			$html.='<option value="'.$key.'">'.$value.'</option>';
	        		}
	        	}else{
	        		$html.='<option value="'.$key.'">'.$value.'</option>';
	        	}
	        }
	        // pr($html);die;
            echo json_encode(array('html' => $html));
        }
	}




	function findEmployee(){
		$this->autoRender=false;
		$this->loadModel('BusinessEmployee');
        if ($this->request->is('Ajax'))
        {
        	$bus_id = $this->data['id'];
        	$data = $this->BusinessEmployee->find('list',array('conditions'=>array('BusinessEmployee.business_id'=>$bus_id),'fields'=>array('id','emp_name'),'order'=>'BusinessEmployee.emp_name ASC'));   
	        asort($data);
            
	        $this->set('emp', $data);
            echo json_encode(array('html' => $data));
        }
	}

	public function logout() {
		return $this->redirect($this->Auth->logout());
	}
public function findState()
    {	
    	$this->autoRender=false;
        if ($this->request->is('Ajax'))
        {
        	$country_id = $this->data['id'];
            $data = $this->Business->State->find('list',array('conditions'=>array('State.country_id'=>$country_id), 'fields'=>array('State.id','stateName'),'order'=>array('stateName ASC')));   
	        $data=array_unique($data);
	        asort($data);
	        //pr($data);die;
	        $this->set('states', $data);
            echo json_encode(array('html' => $data));
        }
    }
    public function findCity()
    {	
    	$this->autoRender=false;
        if ($this->request->is('Ajax'))
        {
        	$state_id = $this->data['id'];
	        $data = $this->Business->City->find('list',array('conditions'=>array('City.state_id'=>$state_id), 'fields'=>array('id','city_name'),'order'=>array('city_name ASC')));  
	        $data=array_unique($data);
	        //sort($data);
	        $this->set('cities', array_unique($data));
            echo json_encode(array('html' => $data));
        }
    }

   function checkEmail_user()
	{   $this->autoRender=false;
	    if ($this->request->is('Ajax'))
        {
	    $this->loadModel('Customer');
            $email = trim($_REQUEST['data']['Customer']['email']);
	    $count = $this->Customer->find('count',array('conditions'=>array('Customer.email'=>$email,'Customer.is_delete'=>0)));
		
		if($count > 0)
		{
			echo "false";die;
		}
		else
		{
			echo "true";die;
		}
	}

	}
     function updatesite($id = null, $status = null)
	{
		$st=$status;
		$this->loadModel('SocialMedia');
		$id= base64_decode($id);
		$data['SocialMedia']['id'] = $id;
		$data['SocialMedia']['status'] = $status;
		
		if($this->SocialMedia->save($data))
		{
			$response[]='success';
             $response[]=base64_encode($id);
             if($st==1){
             	$st=0;
             }else{
  				$st=1;           	
             }
             $response[]=$st;
             $response[]=$id;
             echo json_encode($response);die;
			/*$this->Session->setFlash('Status has been updated successfully.');
			$this->redirect($this->referer());*/
		}

	}	
	public function postReview()
    {    
        if( ($_SERVER['REQUEST_METHOD'] == 'GET' && !empty (base64_decode (@$_GET['id'] ) ) ) || $this->request->is('post') )
        { 
				        $this->layout = 'micro';
				    	$this->loadModel('Business');
				    	$this->loadModel('BusinessReview');
				    	$this->loadModel('Customer');
				    	$this->loadModel('User');

		    	if($this->request->is('post'))
		    	{
				         
				    	$busiess_id = $this->Business->find('first',array('conditions'=>array('Business.user_id'=>$this->data['BusinessReview']['user_id']),'fields'=>array('Business.id','Business.businessname','Business.feedbackthreshold'),'recursive'=>'-1')); 
				    	$threshold = $busiess_id['Business']['feedbackthreshold'];
                                        $business_emial_template_id = $busiess_id['Business']['id']; 
					$business_name = $busiess_id['Business']['businessname'];
					
				    	$user_email = $this->User->find('first',array('contain'=>false,'conditions'=>array('User.id'=>$this->data['BusinessReview']['user_id']),'fields'=>array('User.email'))); 
				        $admin_email = $user_email['User']['email'];
					$this->request->data['BusinessReview']['business_id'] = $busiess_id['Business']['id'];
				    	$desc = strip_tags($this->request->data['BusinessReview']['ratingdescription']);

				    	$this->request->data['BusinessReview']['ratingdescription'] = $desc;
				    	$customer_id = $this->request->data['BusinessReview']['customer_id'];
				        $business_id = $this->request->data['BusinessReview']['business_id']; 
				        $cus_rating_status = $this->request->data['BusinessReview']['ratingstar'].'R';
				        $datetime = date_create()->format('Y-m-d H:i:s');
				        $this->request->data['BusinessReview']['ratingdate'] = $datetime; 
				        $this->loadModel('FeedbackSetting');
					$feedbacksetting=$this->FeedbackSetting->find('first',array('conditions'=>array('FeedbackSetting.business_id'=>$busiess_id['Business']['id']),'recursive'=>-1));
					$this->set('feedbacksetting',$feedbacksetting);
				    	if($this->BusinessReview->save($this->request->data['BusinessReview']))
				    	{         
				    		        $business_review_id = $this->BusinessReview->getLastInsertId();
				    		        
				    		        $this->Customer->updateAll(array('Customer.status' =>"'$cus_rating_status'"), array('Customer.id' => $customer_id,'Customer.business_id'=>$business_id));
				    		        $cus_email = $this->Customer->find('first',array('contain'=>false,'conditions'=>array('Customer.id'=>$customer_id),'fields'=>array('Customer.email','Customer.firstname','Customer.lastname')));
				                        $email = $cus_email['Customer']['email'];
							$name = $cus_email['Customer']['firstname']. ' '. $cus_email['Customer']['lastname'];
							$google = 'www.google.com';
								if($this->request->data['BusinessReview']['ratingstar'] > $threshold)
                                    
                                   {
									$eTemplate=$this->getEmailcontent($business_emial_template_id,3);
									$replace=array('$cusotmername'=>$name,'$customeremail'=>$email,'$business_name'=>$business_name,'$business_email'=>$admin_email);
									$sendername=@$eTemplate['sendername'];
									$sendemail=@$eTemplate['senderemail'];
									$content=$eTemplate['emailcontent'];	
									$subject=@$eTemplate['emailsubject'];
									$receiveremail=$email;
									if($this->_sendingEmail($sendername,$senderemail=0,$receiveremail,$subject,$content,$replace)):
										 
											# send positive alert to business owner
											$eTemplate=$this->getEmailcontent($business_emial_template_id,5);
											$replace=array('$cusotmername'=>$name,'$customeremail'=>$email,'$business_name'=>$business_name,'$business_email'=>$admin_email);
											$sendername=@$eTemplate['sendername'];
											$sendemail=@$eTemplate['senderemail'];
											$content=$eTemplate['emailcontent'];	
											$subject=@$eTemplate['emailsubject'];
											$receiveremail=$admin_email;
											$this->_sendingEmail($sendername,$senderemail=0,$receiveremail,$subject,$content,$replace);
											$this->Session->setFlash('Your Reviews has been shared with us','success');
										  
											$this->redirect(array('controller'=>'admin','action'=>'thanksToCustomer',"?" => array("customer_id" => base64_encode($customer_id),"business_id" => base64_encode($business_id),'business_review_id'=>base64_encode($business_review_id))));
				                     
									endif;
                                             

                      									//$this->redirect(array('controller'=>'dashboard','action'=>'thanksToCustomer',"?" => array("customer_id" => base64_encode($customer_id),"business_id" => base64_encode($business_id),'business_review_id'=>base64_encode($business_review_id))));
				                       }else
				                       {
				                                $eTemplate=$this->getEmailcontent($business_emial_template_id,4);
												$replace=array('$cusotmername'=>$name,'$customeremail'=>$email,'$business_name'=>$business_name,'$business_email'=>$admin_email);
												$sendername=@$eTemplate['sendername'];
												$sendemail=@$eTemplate['senderemail'];
												$content=$eTemplate['emailcontent'];	
												$subject=@$eTemplate['emailsubject'];
												$receiveremail=$email;
												if($this->_sendingEmail($sendername,$senderemail=0,$receiveremail,$subject,$content,$replace)):
													$eTemplate=$this->getEmailcontent($business_emial_template_id,6);
													$replace=array('$cusotmername'=>$name,'$customeremail'=>$email,'$business_name'=>$business_name,'$business_email'=>$admin_email);
													$sendername=@$eTemplate['sendername'];
													$sendemail=@$eTemplate['senderemail'];
													$content=$eTemplate['emailcontent'];	
													$subject=@$eTemplate['emailsubject'];
													$receiveremail=$admin_email;
													$this->_sendingEmail($sendername,$senderemail=0,$receiveremail,$subject,$content,$replace);
													
													$this->redirect(array('controller'=>'admin','action'=>'thanksToNegativeCustomer',"?" => array("customer_id" => base64_encode($customer_id),"business_id" => base64_encode($business_id)))); 
													$this->Session->setFlash('Your Reviews has been shared with us','success');
				                             
												endif;
				                    
                                             }
                        }	
				        else
				        {
				        	
				        }
    			 }
                else 
                {
                	$this->loadModel('BusinessReview');
                        $this->loadModel('Business');
			        $user_id = base64_decode($_GET['id']);
			        $customer_id = base64_decode($_GET['customer_id']);
                                  
			        $agency_logo = $this->Business->find('first',array('conditions'=>array('Business.user_Id'=>@$user_id)));
                        $this->loadModel('FeedbackSetting');
			$feedbacksetting=$this->FeedbackSetting->find('first',array('conditions'=>array('FeedbackSetting.business_id'=>$agency_logo['Business']['id']),'recursive'=>-1));
			$this->set('feedbacksetting',$feedbacksetting);         
                    $cnt = $this->BusinessReview->find('count',array('conditions'=>array('BusinessReview.business_id'=>@$agency_logo['Business']['id'],'BusinessReview.customer_id'=>$customer_id)));
                    if($cnt > 0)
                    {
                    	 
                    	$this->Session->setFlash('You already gave the Reviews..','error');
                    	$this->redirect($this->referer());

                    }
                 
                  

			        $this->set('user_id',@$user_id);
			        $this->set('customer_id',@$customer_id);
                    $this->loadModel('AgencysiteSetting');
			        $conditions = array('AgencysiteSetting.user_id'=>@$agency_logo['Business']['agency_id']);
			        $logo = $this->AgencysiteSetting->find('first',array('contain'=>false,'fields'=>array('AgencysiteSetting.agencylogo'),'conditions'=>$conditions));
			        $this->set('companylogo',@$logo['AgencysiteSetting']['agencylogo']);
			        $this->set('address',@$agency_logo);
                 }
	 }		     
	 else
	 {  
	 	echo "You are not authorized to access this location";die;

	 }    
      
	  
        
    }
    public function thanksToCustomer()
    {
        $customer_id = base64_decode(@$_GET['customer_id']);
        $business_id = base64_decode(@$_GET['business_id']);
        $business_review_id = base64_decode(@$_GET['business_review_id']);
    	if($customer_id && $business_id)
    	{	
		    	$this->layout = 'micro';
		    	$this->loadModel('Business');
		    	$this->loadModel('AgencysiteSetting');
		    	$this->loadModel('Customer');
		    	$this->loadModel('BusinessReview');
		    	$this->loadModel('Visibility');
		    	$sites=$this->Visibility->find('all',array('conditions'=>array('Visibility.business_id'=>$business_id,'Visibility.status'=>'visible','Visibility.pageurl !='=>'')));
		    	$this->set('sites',$sites);
		        $agency_logo = $this->Business->find('first',array('conditions'=>array('Business.id'=>@$business_id)));
		        $conditions = array('AgencysiteSetting.user_id'=>@$agency_logo['Business']['agency_id']);
					        $logo = $this->AgencysiteSetting->find('first',array('contain'=>false,'fields'=>array('AgencysiteSetting.agencylogo'),'conditions'=>$conditions));
			   $customer_name = $this->Customer->find('first',array('contain'=>false,'conditions'=>array('Customer.id'=>$customer_id),'fields'=>array('Customer.firstname','Customer.lastname')));
			    $review_given = $this->BusinessReview->find('first',array('conditions'=>array('BusinessReview.id'=>$business_review_id),'fields'=>array('BusinessReview.ratingdescription')));
			    $this->set('companylogo',@$logo['AgencysiteSetting']['agencylogo']);
			    $this->set('address',@$agency_logo);
			    $this->set('customer_name',@$customer_name);
			    $this->set('review_given',@$review_given);
	   }
	   else{
	   	    echo "You are not authorized to access this location";die;
	   }	
    }
   public function thanksToNegativeCustomer() 
    {

        $customer_id = base64_decode(@$_GET['customer_id']);
        $business_id = base64_decode(@$_GET['business_id']);
        if(($customer_id && $business_id) || $this->request->is('post'))
    	{	
		        $this->layout = 'micro';
		    	$this->loadModel('Business');
		    	$this->loadModel('AgencysiteSetting');
		    	$this->loadModel('Customer');
		        
		        $agency_logo = $this->Business->find('first',array('conditions'=>array('Business.id'=>@$business_id)));
		        $conditions = array('AgencysiteSetting.user_id'=>@$agency_logo['Business']['agency_id']);
					        $logo = $this->AgencysiteSetting->find('first',array('contain'=>false,'fields'=>array('AgencysiteSetting.agencylogo'),'conditions'=>$conditions));
			   $customer_name = $this->Customer->find('first',array('contain'=>false,'conditions'=>array('Customer.id'=>$customer_id),'fields'=>array('Customer.firstname','Customer.lastname')));
			    
			   		        
			    $this->set('companylogo',@$logo['AgencysiteSetting']['agencylogo']);
			    $this->set('address',@$agency_logo);
    		    $this->set('customer_name',@$customer_name);
    		    $this->set('customer_id',@$customer_id);
    		   
			       if($this->request->is('post'))
			       {
			       	$this->loadModel('Customer');
			       		if(!empty($this->data['Customer']['id']))
			       		{	
				    	   	if($this->Customer->save($this->data))
				       		{
				          	$this->Session->setFlash('Your Suggestion Shared with us We Strongly Consider your SUGGESTIONS.','success');
				       		}
				        }
				        else
				        {
				          echo "You are not authorized to access this location";die;	
				        }
			       }

      }
      else{
      	 echo "You are not authorized to access this location";die;
      }			    

 }

 /*Multilocation Managers*/	
 public function manager(){
 	$userType=$this->Session->read('Auth.User.usertype');
 	if($userType == 'admin'){
 		$this->loadModel('BusinessManager');
 		if($this->request->is('post') && isset($this->request->data['searchForm']['search'])){
 			  $search = $this->request->data['searchForm']['search'];
			  $search = trim($search);
			  $this->paginate = array('limit'=>'15',
					    'conditions' => array('User.usertype'=>'multilocationmanager',
					    	'OR'=>array('BusinessManager.name LIKE' => '%'.$search.'%')));
 		}else{
 			$this->paginate=array('limit'=>'15','conditions'=>array('User.usertype'=>'multilocationmanager'));
 		}
 		$this->set('managers',$this->paginate('BusinessManager'));
 		//pr($this->paginate('BusinessManager'));die;
 	}else{
 		$this->Session->setFlash('You are not authorised user to access that location.');
 		$this->redirect($this->referer());
 	}	
 }

 public function addmanager(){
		$userType=$this->Session->read('Auth.User.usertype');
 		if($userType == 'admin'){
 			$this->loadModel('User');
 			$this->loadModel('BusinessManager');
			if($this->request->is('post')){
			 $agency_list = $this->User->find('all',array('conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>''),'recursive'=>-1,'order'=>'agencyname ASC'));
				$this->set('agnc',$agency_list);	
             $errors=$this->validateManager($this->request->data);
             if(empty($errors)){
             	$this->request->data['User']['usertype']='multilocationmanager';
             	if(isset($this->request->data['User']['status']) && $this->request->data['User']['status']=='on'){
             		$this->request->data['User']['status']=1;
             	}else{
             		$this->request->data['User']['status']=0;
             	}
             if($this->User->save($this->request->data['User'])){
             	$uid=$this->User->find('first',array('contain'=>false,'conditions'=>array('User.email'=>$this->request->data['User']['email']),'fields'=>array('User.id')));
             	$this->request->data['BusinessManager']['user_id']=$uid['User']['id'];
             	$this->request->data['BusinessManager']['name']=$this->request->data['User']['firstname'].' '.$this->request->data['User']['lastname'];
             	if($this->BusinessManager->save($this->request->data['BusinessManager'])){
             		$this->Session->setFlash('Manager has been created successfully.','success');
             		$this->redirect(array('controller'=>'admin','action'=>'manager'));
             	}else{
             		$this->Session->setFlash('The manager could not be saved. Please, try again.','error');
              		$this->redirect($this->referer());
             	}
              }else{
              	$this->Session->setFlash('The manager could not be saved. Please, try again.','error');
              	$this->redirect($this->referer());
              }
             }else{
             	$this->set('errors',$errors);
             	$this->set('data',$this->request->data);
             }
			}else{
				$agency_list = $this->User->find('all',array('conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>''),'recursive'=>-1,'order'=>'agencyname ASC'));
				$this->set('agnc',$agency_list);
			}
 		}else{
 			$this->Session->setFlash('You are not authorised user to access that location.','error');
 			$this->redirect($this->referer());
 		}
 }

 public function validateManager($data){
 	$reg='/^[a-zA-Z ]+$/';
 	$error=array();
    if(empty($data['User']['firstname'])){
    	$error['User']['firstname'] = 'Please enter your first name.';
    }
    if(!preg_match($reg,$data['User']['firstname']) && !empty($data['User']['firstname'])){
    	$error['User']['firstname'] = 'Please enter character only.';
    }
    if(!preg_match($reg,$data['User']['lastname']) && !empty($data['User']['lastname'])){
    	$error['User']['lastname'] = 'Please enter character only.';
    }
    if(empty($data['User']['password'])){
    	$error['User']['password'] = 'Please enter your password.';
    }
    if(strlen($data['User']['password'])<8 && !empty($data['User']['password'])){
    	$error['User']['password'] = 'Password should be atleast 8 characters long.';
    }
    if(empty($data['User']['cpassword'])){
    	$error['User']['cpassword'] = 'Please enter confirm password.';
    }
    if(trim($data['User']['cpassword'])!=trim($data['User']['password']) && !empty($data['User']['cpassword']) && !empty($data['User']['password'])){
    	$error['User']['cpassword'] = 'Password do not match.';
    }
    if(empty($data['BusinessManager']['agency_id'])){
    	$error['BusinessManager']['agency_id'] = 'Please select the agency.';
    }
   if(empty($data['BusinessManager']['businessIds'])){
    	$error['BusinessManager']['businessIds'] = 'Please select businesses.';
    }
    if(!empty($data['BusinessManager']['businessIds']) && isset($data['BusinessManager']['businessIds'][0]) && !@$data['BusinessManager']['businessIds'][0]){
    	$error['BusinessManager']['businessIds'] = 'Please select businesses.';
    }

     if(empty($data['User']['email'])){
            $error['User']['email'] = 'Please enter the  email.';
       }
    if (!filter_var($data['User']['email'], FILTER_VALIDATE_EMAIL) && !empty($data['User']['email'])){
          $error['User']['email'] = "Invalid email format.";
    }
    $regex = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i"; 
    if (!preg_match($regex, $data['User']['email']) && !empty($data['User']['email'])) {
      $error['User']['email'] = "Invalid email format.";
    }
    return $error;
 }

 public function validbus(){
 	$data=$_REQUEST['data'];
 	if(isset($data['BusinessManager']['businessIds'][0]) && !$data['BusinessManager']['businessIds'][0]){
 		echo "false";die;
 	}else{
 		echo "true";die;
 	}
 }

 public function updatestatusmanager($id = null, $status = null){
 	$st=$status;
 	$id=base64_decode($id);
 	$data['User']['id']=$id;
 	$data['User']['status']=$status;
 	$this->loadModel('User');
 	if($this->User->save($data)){
 		 $response[]='success';
	     $response[]=base64_encode($id);
	     if($st==1){
	     	$st=0;
	     }else{
				$st=1;           	
	     }
	     $response[]=$st;
	     $response[]=$id;
 	}else{
 		$response[]='error';
	     $response[]=base64_encode($id);
	     if($st==1){
	     	$st=0;
	     }else{
				$st=1;           	
	     }
	     $response[]=$st;
	     $response[]=$id;
 	}
     echo json_encode($response);die;
 }
 public function editmanager($id=null){
 	$userType=$this->Session->read('Auth.User.usertype');
 	if($userType == 'admin'){
	 	$this->loadModel('User');
	 	$agency_list = $this->User->find('all',array('conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>''),'recursive'=>-1,'order'=>'agencyname ASC'));
		$this->set('agnc',$agency_list);
	 	if($id){
	 		$mid=base64_decode($id);
	 		$this->loadModel('BusinessManager');
	 		$manager=$this->BusinessManager->findById($mid);
	 		if(!empty($manager)){
	 			$this->loadModel('Business');
        		$agency_id = $manager['BusinessManager']['agency_id'];
        	    $data = $this->Business->find('all',array('contain'=>array('User'),'conditions'=>array('Business.agency_id'=>$agency_id,'Business.is_deleted'=>0,'User.status'=>1,'Business.agency_id >'=>0), 'fields'=>array('id','businessname'),'order'=>array('businessname ASC'))); 
	        	$business=array();
		        foreach ($data as $key => $value) {
		        $business[$value['Business']['id']]=$value['Business']['businessname'];
		        }
		        asort($business);
		        $this->set('business', $business);
	 			$this->set('manager',$manager);
	 		}else{
	 			throw new NotFoundException();
	 		}
	 	}elseif($this->request->is('post')) {
	 		$this->loadModel('Business');
    		$agency_id = $this->request->data['BusinessManager']['agency_id'];
    	    $data = $this->Business->find('all',array('contain'=>array('User'),'conditions'=>array('Business.agency_id'=>$agency_id,'Business.is_deleted'=>0,'User.status'=>1,'Business.agency_id >'=>0), 'fields'=>array('id','businessname'),'order'=>array('businessname ASC'))); 
        	$business=array();
	        foreach ($data as $key => $value) {
	        $business[$value['Business']['id']]=$value['Business']['businessname'];
	        }
	        asort($business);
	        $this->set('business', $business);
	 		$errors=$this->validateEditManager($this->request->data);
	 		if(empty($errors)){
	 			$this->request->data['BusinessManager']['name']=trim($this->request->data['User']['firstname']).' '.trim($this->request->data['User']['lastname']);
	 			if(isset($this->request->data['User']['status'])){
	 				$this->request->data['User']['status']=1;
	 			}else{
	 				$this->request->data['User']['status']=0;
	 			}
	 			$this->loadModel('BusinessManager');
	 			if($this->BusinessManager->saveAll($this->request->data)){
	 				$this->Session->setFlash('Manager has been updated successfully.','success');
 					$this->redirect(array('controller'=>'admin','action'=>'manager'));
	 			}else{
	 				$this->Session->setFlash('!Opps, there is someting wrong. Please try again after some time.','error');
 					$this->redirect($this->referer());
	 			}
	 		}else{
	 			$this->set('errors',$errors);
             	$this->set('manager',$this->request->data);
	 		}
	 	}else{
	 		throw new NotFoundException();
	 	}
	}else{
		$this->Session->setFlash('You are not authorised user to access that location.','error');
 		$this->redirect($this->referer());
	} 	
 }
  public function validateEditManager($data){
 	$reg='/^[a-zA-Z ]+$/';
 	$error=array();
    if(empty($data['User']['firstname'])){
    	$error['User']['firstname'] = 'Please enter your first name.';
    }
    if(!preg_match($reg,$data['User']['firstname']) && !empty($data['User']['firstname'])){
    	$error['User']['firstname'] = 'Please enter character only.';
    }
    if(!preg_match($reg,$data['User']['lastname']) && !empty($data['User']['lastname'])){
    	$error['User']['lastname'] = 'Please enter character only.';
    }
  
    if(empty($data['BusinessManager']['agency_id'])){
    	$error['BusinessManager']['agency_id'] = 'Please select the agency.';
    }
   if(empty($data['BusinessManager']['businessIds'])){
    	$error['BusinessManager']['businessIds'] = 'Please select businesses.';
    }
    if(!empty($data['BusinessManager']['businessIds']) && isset($data['BusinessManager']['businessIds'][0]) && !@$data['BusinessManager']['businessIds'][0]){
    	$error['BusinessManager']['businessIds'] = 'Please select businesses.';
    }

     if(empty($data['User']['email'])){
            $error['User']['email'] = 'Please enter the  email.';
       }
    if (!filter_var($data['User']['email'], FILTER_VALIDATE_EMAIL) && !empty($data['User']['email'])){
          $error['User']['email'] = "Invalid email format.";
    }
    $regex = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i"; 
    if (!preg_match($regex, $data['User']['email']) && !empty($data['User']['email'])) {
      $error['User']['email'] = "Invalid email format.";
    }
    return $error;
 }

 /*Partner Update*/
  public function partner(){
  	$userType=$this->Session->read('Auth.User.usertype');
 	if($userType == 'admin'){
	  	$this->loadModel('Partner');
	  	if($this->request->is('post') && isset($this->request->data['searchForm']['search'])){
 			  $search = $this->request->data['searchForm']['search'];
			  $search = trim($search);
			  $this->paginate = array('limit'=>'15',
					    'conditions' => array('Partner.status'=>1,
					    	'OR'=>array('Partner.agencyname LIKE' => '%'.$search.'%')));
 		}else{
	  		$this->paginate=array('limit'=>'15','conditions'=>array('Partner.status'=>1));
	  	}
	  	$partners=$this->paginate('Partner');
	  	//pr($partners);die;
	  	$this->set(compact('partners'));
	}else{
		$this->Session->setFlash('You are not authorised user to access that location.');
 		$this->redirect($this->referer());
	}  	
  }
  
  /*Add partner from existing agency*/

  public function addpartner(){
	    $userType=$this->Session->read('Auth.User.usertype');
	 	if($userType == 'admin'){
	 		$this->loadModel('User');
	 		$agency_list = $this->User->find('all',array('contain'=>array('Partner'),'conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>'','Partner.id'=>''),'order'=>'agencyname ASC'));
			$this->set('agnc',$agency_list);
			if($this->request->is('post')){
				$data=$this->request->data;
				$data['User']['id']=$data['Partner']['user_id'];
				$data['Partner']['status']=1;
				$this->loadModel('Partner');
				$errors=$this->validate_partner($data);
				if(empty($errors)){
				   if($this->Partner->saveAll($data)){
						$this->Session->setFlash('Partner has been added successfully.','success');
						$this->redirect(array('controller'=>'admin','action'=>'partner'));
					}else{
						throw new NotFoundException();
					}
				}else{
					$this->set(compact('errors','data'));
				}
			}
	 	}else{
	 		$this->Session->setFlash('You are not authorised user to access that location.');
 			$this->redirect($this->referer());
	 	}	
  }

  
  /*Add new partner as new agency*/

  public function newPartner(){
  		$userType=$this->Session->read('Auth.User.usertype');
	 	if($userType == 'admin'){
	 		 if($this->request->is('post')){
	 		 	$this->loadModel('User');
	 		 	$data=$this->request->data;
	 		 	$data['User']['usertype']="reseller";
	 		 	$errors=$this->validate_newPartner($data);
	 		 	$data['Partner']['status']=1;
	 		 	if(empty($errors)){
	 		 		if($this->User->saveAll($data)){
		 		 		$this->Session->setFlash('Partner has been added successfully.','success');
		 		 		$this->redirect(array('controller'=>'admin','action'=>'partner'));
		 		 	}else{
		 		 		throw new NotFoundException();
		 		 	}
		 		 }else{
		 		 	$this->set('errors',$errors);
		 		 	$this->set('data',$data);
		 		 }
	 		 }
	 	}else{
	 		$this->Session->setFlash('You are not authorised user to access that location.');
 			$this->redirect($this->referer());
	 	}	
  }

   /*Add agency under partner*/
  public function addAgency($id=null){
  	$userType=$this->Session->read('Auth.User.usertype');
	 	if($userType == 'admin'){
		 	if($id){
		 		$this->loadModel('Partner');
		 	    $partners=$this->Partner->find('first',array('conditions'=>array('Partner.id'=>base64_decode($id))));
		 		if($this->request->is('post')){
	 		 	$this->loadModel('User');
	 		 	$data=$this->request->data;
	 		 	$data['User']['usertype']="reseller";
	 		 	$errors=$this->validate_newPartner($data);
	 		 	if(empty($errors)){
	 		 		if($this->User->save($data)){
	 		 			$userId=$this->User->getLastInsertId();
	 		 			if(empty($partners['Partner']['agencyIds'])){
	 		 				$ids=array();
	 		 				array_push($ids, $userId);
	 		 				$partners['Partner']['agencyIds']=$ids;
	 		 			}else{
	 		 				array_push($partners['Partner']['agencyIds'], $userId);
	 		 			}
	 		 			$data['AgencysiteSetting']['user_id']=$userId;
	 		 			$this->Partner->save($partners);
	 		 			$this->loadModel('AgencysiteSetting');
	 		 			$this->AgencysiteSetting->save($data['AgencysiteSetting']);
		 		 		$this->Session->setFlash('Agency has been added successfully.','success');
		 		 		$this->redirect(array('controller'=>'admin','action'=>'partner'));
		 		 	}else{
		 		 		throw new NotFoundException();
		 		 	}
		 		 }else{
		 		 	$this->set('errors',$errors);
		 		 	$this->set('data',$data);
		 		 }
	 		 }
	 		 $this->set(compact('partners'));
		 	}else{
		 		throw new NotFoundException();
		 	} 
	 	}else{
	 		$this->Session->setFlash('You are not authorised user to access that location.');
 			$this->redirect($this->referer());
	 	}
  }


/*Validate partners*/
  public function validate_newPartner($data){
  	$reg='/^[a-zA-Z ]+$/';
    $error=array();
    if(empty($data['User']['firstname'])){
        $error['User']['firstname'] = 'Please enter your first name.';
    }
    if(!preg_match($reg,$data['User']['firstname']) && !empty($data['User']['firstname'])){
        $error['User']['firstname'] = 'Please enter character only.';
    }
    if(!preg_match($reg,$data['User']['lastname']) && !empty($data['User']['lastname'])){
        $error['User']['lastname'] = 'Please enter character only.';
    }
    if(empty($data['AgencysiteSetting']['agencyname'])){
        $error['agency']['agencyname'] = 'Please enter your agency name.';
    }
    if(!preg_match($reg,$data['AgencysiteSetting']['agencyname']) && !empty($data['AgencysiteSetting']['agencyname'])){
        $error['agency']['agencyname'] = 'Please enter character only.';
    }
    if(empty($data['User']['password'])){
        $error['User']['password'] = 'Please enter your password.';
    }
    if(strlen($data['User']['password'])<8 && !empty($data['User']['password'])){
        $error['User']['password'] = 'Password should be atleast 8 characters long.';
    }
    if(empty($data['User']['cpassword'])){
        $error['User']['cpassword'] = 'Please enter confirm password.';
    }
    if(trim($data['User']['cpassword'])!=trim($data['User']['password']) && !empty($data['User']['cpassword']) && !empty($data['User']['password'])){
        $error['User']['cpassword'] = 'Password do not match.';
    }

     if(empty($data['User']['email'])){
            $error['User']['email'] = 'Please enter the  email.';
       }
    if (!filter_var($data['User']['email'], FILTER_VALIDATE_EMAIL) && !empty($data['User']['email'])){
          $error['User']['email'] = "Invalid email format.";
    }
    $regex = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i"; 
    if (!preg_match($regex, $data['User']['email']) && !empty($data['User']['email'])) {
      $error['User']['email'] = "Invalid email format.";
    }
    return $error;
 }
 public function validate_partner($data){
	$reg='/^[a-zA-Z ]+$/';
    $error=array();
     if(empty($data['Partner']['user_id'])){
    	$error['Partner']['user_id'] = 'Please select Agency.';
    }
    if(!empty($data['Partner']['user_id']) && isset($data['Partner']['user_id'][0]) && !@$data['Partner']['user_id'][0]){
    	$error['Partner']['user_id'] = 'Please select businesses.';
    }
    
    return $error;
 }
  /*Start:Add partner business updated by M*/
  	function newbusiness($id=null){
	$userType=$this->Session->read('Auth.User.usertype');
	if($userType == 'admin'){
		$this->set("id",$id);
		$this->loadModel('BusinessCategory');
        $this->loadModel('Partner');
		$this->loadModel('User');
		$businessCategories = $this->BusinessCategory->find('list',array('conditions'=>array('BusinessCategory.status'=>1),'fields'=>array('id','name'),'recursive'=>-1,'order'=>'name ASC'));
		$partners=$this->Partner->find('first',array('conditions'=>array('Partner.id'=>base64_decode($id))));
		$agIds=$this->Partner->find('first',array('contain'=>false,'conditions'=>array('Partner.id'=>base64_decode($id)),'fields'=>array('Partner.agencyIds')));
		$agency_list = $this->User->find('all',array('contain'=>false,'conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>'','User.id'=>$agIds['Partner']['agencyIds']),'order'=>'agencyname ASC'));
        $this->set(compact('businessCategories','agency_list','partners'));
		if($this->request->is('post')){
			$errors=$this->validate_bussiness($this->request->data);
		    if(empty($errors)){
		        if(!empty($this->data)){
		            $data= $this->data;
					$this->loadModel('User');
					$this->loadModel('Business');
					$data['User']['usertype'] = 'subscriber';
						if($data['User']['status'] ==''){
							$data['User']['status']=0;
						}
						if($this->User->save($data)){
							$id = $this->User->getLastInsertId();
							$data['Business']['user_Id'] = $id;
		                    $data['Business']['createdat']=Date('Y-m-d');
							if($this->Business->save($data)){
								$this->Session->setFlash('Business added successfully.','success');
			 					$this->redirect('/admin/agencyBusiness');
							}
						}

					}
		    }else{
		            $this->set('errors',$errors);
		            $this->set('data',$this->request->data);
		    }
	    }
    }else{
	 		$this->Session->setFlash('You are not authorized user to access that location.','error');
	 		$this->redirect($this->referer());
 		}
	}

	function validate_bussiness($data){

	$reg='/^[a-zA-Z ]+$/';
 	$error=array();
 	if(empty($data['Business']['agency_id'])){
    	$error['Business']['agency_id'] = 'Please select Agency.';
    }
    if(!empty($data['Business']['agency_id']) && isset($data['Business']['agency_id'][0]) && !@$data['Business']['agency_id'][0]){
    	$error['Business']['agency_id'] = 'Please select Agency.';
    }

    if(empty($data['Business']['business_category_id'])){
    	$error['Business']['business_category_id'] = 'Please select Bussiness categoery.';
    }
    if(!empty($data['Business']['business_category_id']) && isset($data['Business']['business_category_id'][0]) && !@$data['Business']['business_category_id'][0]){
    	$error['Business']['business_category_id'] = 'Please select Agency.';
    }

    if(empty($data['Business']['businessname'])){
    	$error['Business']['businessname'] = 'Please enter the bussiness name.';
    }
    
     if(empty($data['User']['email'])){
            $error['User']['email'] = 'Please enter the  email.';
       }
    if (!filter_var($data['User']['email'], FILTER_VALIDATE_EMAIL) && !empty($data['User']['email'])){
          $error['User']['email'] = "Invalid email format.";
    }
    $regex = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i"; 
    if (!preg_match($regex, $data['User']['email']) && !empty($data['User']['email'])) {
      $error['User']['email'] = "Invalid email format.";
    }
    if(empty($data['User']['password'])){
        $error['User']['password'] = 'Please enter your password.';
    }
    if(strlen($data['User']['password'])<8 && !empty($data['User']['password'])){
        $error['User']['password'] = 'Password should be atleast 8 characters long.';
    }
    if(empty($data['User']['cpassword'])){
        $error['User']['cpassword'] = 'Please enter confirm password.';
    }
    if(trim($data['User']['cpassword'])!=trim($data['User']['password']) && !empty($data['User']['cpassword']) && !empty($data['User']['password'])){
        $error['User']['cpassword'] = 'Password do not match.';
    }
    return $error;
	}
  /*End*/

  function addMultilocationManager(){
 	$this->loadModel('User');
 	$userType=$this->Session->read('Auth.User.usertype'); 
 	$agency_list = $this->User->find('all',array('contain'=>array('MultilocationManager'),'conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>'','MultilocationManager.id'=>''),'order'=>'agencyname ASC'));
 	$this->set('agnc',$agency_list);
	if($this->request->is('post')){
 	    if($userType == 'admin'){
 	        $errors=$this->validate_manager($this->request->data);
			if(empty($errors)){
			 	if(!empty($this->data)){
			 		$data = $this->data;
					$data['User']['usertype'] = 'reseller';
					if(isset($data['User']['status']) && $data['User']['status'] !=''){
						$data['User']['status']=1;
					}else{
						$data['User']['status']=0;
					} 
					$data['AgencysiteSetting']['agencyname'] = $data['agency']['agencyname'];
		 			$data['MultilocationManager']['agency_id'] = $data['agency']['user_id'];
		 			$this->User->unbindModel(array('hasMany' => array('MultilocationManager')));
		 			if($this->User->saveAll($data)){ 
		                $this->Session->setFlash('Agency added successfully.','success');
		 				$this->redirect('/admin/mmanager');
		 			}
		 		}
	        }else{
			$this->set('errors',$errors);
			$this->set('data',$this->request->data);
	        }
   		}
    }
}

function validate_manager($data){

	$reg='/^[a-zA-Z ]+$/';
 	$error=array();
 	if(empty($data['agency']['user_id'])){
    	$error['agency']['user_id'] = 'Please select Agency.';
    }
    if(!empty($data['agency']['user_id']) && isset($data['agency']['user_id'][0]) && !@$data['agency']['user_id'][0]){
    	$error['agency']['user_id'] = 'Please select Agency.';
    }
    if(empty($data['User']['firstname'])){
        $error['User']['firstname'] = 'Please enter your first name.';
    }
    if(!preg_match($reg,$data['User']['firstname']) && !empty($data['User']['firstname'])){
        $error['User']['firstname'] = 'Please enter character only.';
    }
    if(!preg_match($reg,$data['User']['lastname']) && !empty($data['User']['lastname'])){
        $error['User']['lastname'] = 'Please enter character only.';
    }

    if(empty($data['agency']['agencyname'])){
    	$error['agency']['agencyname'] = 'Please enter the bussiness name.';
    }
    
     if(empty($data['User']['email'])){
            $error['User']['email'] = 'Please enter the  email.';
       }
    if (!filter_var($data['User']['email'], FILTER_VALIDATE_EMAIL) && !empty($data['User']['email'])){
          $error['User']['email'] = "Invalid email format.";
    }
    $regex = "/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i"; 
    if (!preg_match($regex, $data['User']['email']) && !empty($data['User']['email'])) {
      $error['User']['email'] = "Invalid email format.";
    }
    if(empty($data['User']['password'])){
        $error['User']['password'] = 'Please enter your password.';
    }
    if(strlen($data['User']['password'])<8 && !empty($data['User']['password'])){
        $error['User']['password'] = 'Password should be atleast 8 characters long.';
    }
    if(empty($data['User']['cpassword'])){
        $error['User']['cpassword'] = 'Please enter confirm password.';
    }
    if(trim($data['User']['cpassword'])!=trim($data['User']['password']) && !empty($data['User']['cpassword']) && !empty($data['User']['password'])){
        $error['User']['cpassword'] = 'Password do not match.';
    }
    return $error;
	}

	/*function mmanager(){
 	$this->loadModel('User');
    $this->paginate = array('limit'=>'15',	
                      'conditions' => array('MultilocationManager.user_id <>'=>NULL),'contain'=>array('MultilocationManager'),'order'=>array('MultilocationManager.id'=>'DESC'));
    $this->set('multimanager',$this->paginate('User'));
 	}*/

 	function mmanager(){
	 	$this->loadModel('User');
	    $this->paginate = array('limit'=>'15',	
	                      'conditions' => array('MultilocationManager.user_id <>'=>NULL),'contain'=>array('MultilocationManager'),'order'=>array('MultilocationManager.id'=>'DESC'));
	    $this->set('multimanager',$this->paginate('User'));
	    if($this->request->is('post'))
		        {                       
	                $this->loadModel('User');
		        	$data = $this->data;
		            if(isset($data['searchForm']['search']) && !empty($data['searchForm']['search'])){  
	                    $search = $data['searchForm']['search'];
						$search = trim($search);
						$this->paginate = array(
						'conditions' => array('MultilocationManager.user_id <>'=>NULL,array('User.agencyname LIKE' => '%'.$search.'%')));	
					    $this->set('multimanager',$this->paginate('User'));
					    $this->set('searchText',$search);
	                }
		 	}
	}

 public function multilocationstatus( $id = null, $status = null ){
      		$this->loadModel('User');
      		$this->loadModel('MultilocationManager');
      		$id= base64_decode($id);
      		$multilocationmanager_data = $this->MultilocationManager->find('first',array(
      			'conditions'=>array('MultilocationManager.user_id'=>$id),'fields'=>array('agency_id')));
      		$checkAgencyStatus = $this->User->find('first',array('conditions'=>array('User.id'=>$multilocationmanager_data['MultilocationManager']['agency_id']),'fields'=>array('status'),'contain'=>false));
      		if( $checkAgencyStatus['User']['status'] == 0 )
      		{
      		   $response[]='agencyDeactivated';
	           echo json_encode($response);die;
      		}
      		else
      		{
      			$multilocationmanager['User']['id'] = $id;
      			$multilocationmanager['User']['status'] = $status;
      			$multilocationmanager['User']['stage'] = $multilocationmanager['User']['status'];
      			if( $this->User->save( $multilocationmanager['User'] ) )
      			{
      				 
				$response[]='success';
             	$response[]=base64_encode($id);
             	if( $status==1 ){
             	$status=0;
             	}else{
  				$status=1;           	
             	}
             	$response[]=$status;
             	$response[]=$id;
             	echo json_encode($response);die;
                }

      		}	


  	}
  	/* this new function updated by vipin 8 May 2015 */
	public function updatestatusagencyPartner( $id = null, $status = null ) {
	    $st=$status;
		$this->loadModel('User');
		$this->loadModel('Business');
		$this->loadModel('BusinessEmployee');
		$this->loadModel('Partner');
		$id= base64_decode($id);
		$agency['User']['id'] = $id;
		$agency['User']['status'] = $status;

		if($this->User->save($agency))
		{
           $partner_data = $this->Partner->find('first',array('conditions'=>array('Partner.user_id'=>$id),'fields'=>array('Partner.agencyIds')));
         for( $index = 0;$index < count( @$partner_data['Partner']['agencyIds'] ); $index++ )
          {
          	$agencydatastatus = $this->User->find('first',array('conditions'=>array('User.id'=>@$partner_data['Partner']['agencyIds'][$index]),'fields'=>array('status','stage')));

                if($status==1){
		            			if(isset($agencydatastatus['User']['stage'])) {
		            				$agency['User']['status']=$agencydatastatus['User']['stage'];
		            				$agency['User']['stage'] = $agencydatastatus['User']['status'];
			            		}else{
			            			$agency['User']['status']=0;
			            			$agency['User']['stage']= @$agencydatastatus['User']['status'];
			            		}
		            	    }else{
		            	    	$agency['User']['status'] = $status;
		            	    	$agency['User']['stage'] = @$agencydatastatus['User']['status'];
		            	    } 
            $agency['User']['id'] = @$partner_data['Partner']['agencyIds'][$index];
            $this->User->save(@$agency);
          }           

       for( $index = 0;$index < count( @$partner_data['Partner']['agencyIds'] ); $index++ )
       {
           $agency_data = $this->User->find('first',array('conditions'=>array('User.id'=>@$partner_data['Partner']['agencyIds'][$index]),'recursive'=> -1));
            if(@$agency_data['User']['usertype'] == 'reseller' && @$agency_data['User']['status'] == $status)
            {
            	$conditions = array('Business.agency_id'=>@$agency_data['User']['id']);
            	$agency_business = $this->Business->find('all',array('contain'=>array('User'),'conditions'=>$conditions));
            	foreach (@$agency_business as $key => $value) 
            	{
            		$business['User']['id'] = @$value['Business']['user_Id'];
            		if($status==1){
            			if(isset($value['Business']['stage']) && $value['Business']['stage']){
            			$business['User']['status']=$value['Business']['stage'];
	            		}else{
	            			$business['User']['status']=0;
	            		}
            		}else{
            			$business['User']['status'] = $status;
            		}
            		if($this->User->save(@$business))
            		{
            			if($status==0){
            				$value['Business']['stage']=$value['User']['status'];
            				$this->Business->save($value);
            			}
            			$conditions = array('BusinessEmployee.business_id'=>$value['Business']['id']);
            			$business_employee = $this->BusinessEmployee->find('all',array('contain'=>array('User'),'conditions'=>$conditions));
            			foreach (@$business_employee as $key1 => $value1) 
            			{
            				
            			  	$employee['User']['id'] = @$value1['BusinessEmployee']['user_id'];
            			  	if($status==1){
		            			if(isset($value1['BusinessEmployee']['stage']) && $value1['BusinessEmployee']['stage']){
		            				$employee['User']['status']=$value1['BusinessEmployee']['stage'];
			            		}else{
			            			$employee['User']['status']=0;
			            		}
		            	    }else{
		            	    	$employee['User']['status'] = $status;
		            	    }
	            		    if($this->User->save(@$employee)){
		            		      	if($status==0){
			            				$value1['BusinessEmployee']['stage']=$value1['User']['status'];
			            				$this->BusinessEmployee->save($value1);
		            				}
	            		    }

            			}
            		}
        	    }

            }
        }
             $response[]='success';
             $response[]=base64_encode($id);
             if($st==1){
             	$st=0;
             }else{
  				$st=1;           	
             }
             $response[]=$st;
             $response[]=$id;
             echo json_encode($response);die;
          
		}

	}

    public function editMultilocationManager($id=null){
    	$id=base64_decode($id);
    	$this->loadModel('User');
    	$data=$this->User->findById($id);
    	$agnc = $this->User->find('all',array('contain'=>array('MultilocationManager'),'conditions'=>array('User.usertype'=>'reseller','User.status'=>1,'User.agencyname <>'=>'','MultilocationManager.id'=>''),'order'=>'agencyname ASC'));
    	$this->set(compact('data','agnc'));
    	if($this->request->is('post')){
    		$data=$this->request->data;
    		if($this->User->save($data['User'])){
    			$this->loadModel('AgencysiteSetting');
    			$this->AgencysiteSetting->save($data['AgencysiteSetting']);
    			$this->Session->setFlash('Multilocation Manager has been updated successfully','success');
    			$this->redirect(array('controller'=>'admin','action'=>'mmanager'));
    		}
    	}

    }

    public function checkExistEmail($id=null){
    	if($id){
    		$this->loadModel('User');
    		$user=$this->User->find('first',array('conditions'=>array('User.email'=>$_REQUEST['data']['User']['email'],'User.id <>'=>$id)));
    		if(count($user)>0){
    			echo "false";die;
    		}else{
    			echo "true";die;
    		}
    	}
    }	
}
?>
