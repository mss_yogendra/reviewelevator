<?php 
App::uses('AppController', 'Controller');
class ManagerController extends AppController {
	function beforeFilter(){
            parent::beforeFilter();
    }
	public function index(){
	 if($this->Session->read('Auth.User.usertype')=='multilocationmanager'){	
		$this->loadModel('BusinessManager');
		$manager=$this->BusinessManager->find('first',array('conditions'=>array('BusinessManager.user_id'=>$this->Session->read('Auth.User.id'))));
		if(isset($this->request->data['searchForm']['search'])){
				$searchValue=$this->request->data['searchForm']['search'];
				$searchValue = trim($searchValue);
				$this->loadModel('Business');
				$this->Business->recursive = 0;
				$this->paginate = array(
						    'conditions' => array(
						     'Business.is_deleted'=>0,	
						     'Business.id'=>$manager['BusinessManager']['businessIds'],'Business.businessname LIKE' => "%$searchValue%"));
				$this->set('businesses',$this->paginate('Business'));
	            $this->set('searchText',$searchValue);
				$businessesdata=$this->Business->find('all',array('contain'=>array('User'),'order'=>array('Business.id'=>'DESC'),'fields'=>array('Business.id','Business.businessname','Business.totalReviews','Business.lastReviewdate','Business.averageRating'),'conditions'=>array('Business.id'=>$manager['BusinessManager']['businessIds'],'Business.is_deleted'=>0,'Business.totalReviews !='=>0)));
				$this->set('businessesdata',$businessesdata);	
			}else{
				$this->loadModel('Business');
				$this->Business->recursive = 0;
				$this->paginate = array('limit'=>'15','order'=>array('Business.id'=>'DESC'),'conditions'=>array('Business.id'=>$manager['BusinessManager']['businessIds'],'Business.is_deleted'=>0));
				$this->set('businesses',$this->paginate('Business'));	
				$businessesdata=$this->Business->find('all',array('contain'=>array('User'),'order'=>array('Business.id'=>'DESC'),'fields'=>array('Business.id','Business.businessname','Business.totalReviews','Business.lastReviewdate','Business.averageRating'),'conditions'=>array('Business.id'=>$manager['BusinessManager']['businessIds'],'Business.is_deleted'=>0,'Business.totalReviews !='=>0)));
				$this->set('businessesdata',$businessesdata);	
			}
		}else{
			$this->Session->setFlash('You are not authorized user to access that location.','error');
			$this->redirect($this->referer());
		}	
	}
}
?>
