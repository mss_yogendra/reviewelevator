<?php 
App::uses('AppController', 'Controller');

class ApisController extends AppController {
		
		public $error=array();

		function beforeFilter() {
			#parent::beforeFilter();
			$this->Auth->allow('registerAgency','Agencypayment_Status','getAgencycount');
		}
		 
		public function generateRandomString($length = 10) {
			    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			    $charactersLength = strlen($characters);
			    $randomString = '';
			    for ($i = 0; $i < $length; $i++) {
			        $randomString .= $characters[rand(0, $charactersLength - 1)];
			    }
			    return $randomString;
			}
		public function validateToken($token=null){
			if($token==='WwZE\w\nZg(2T35@qt8pb<z=5H~Jc^'){
				return true; 
			}
			return false;
		}

		##############################################################################
		############################## Create An Agency user##########################
		###						Request parameter 										
		###					      1. token 
		####   					  2. firstname
		####				      3. lastname
		#####				      4. email
		####			          5. agencyname
		###				          6. addressline1
		#####				      7. addressline2
		######			          8. city
		######			          9. zip
		######				     10.country
		######			         11.phone
		######			         12.allowedbusiness
		###############################################################################
		################################################################################

		public function registerAgency(){

			$response=array();
			$request=$this->request->data;			
			if(empty(@$request['token']) || !$this->validateToken(@$request['token'])){
				http_response_code(401); exit( 'Not authorized' );
			}

			if(isset($request)):
					$this->_validateinput($request);
					if(count($this->error)>0):
						$response['error']=$this->error;
						$response['success']=0;
						$response['message']='Please fix the error and try again!!!';
					else:
						$this->loadModel('User');
 						$this->loadModel('AgencysiteSetting');
 						$data = $this->data;
 						$agencyemail=trim($request['email']);
 						#check for the Agency user exist or not as per his email id
 						$udata= $this->User->find('all',array('fields'=>array('id','email'),'conditions'=>array('User.email'=>@$agencyemail),'recursive'=>-1));
 						if(count($udata)>0):
 							//user already exists with the current email id
 							$response['success']=0;
					 		$response['message']='Agency already exists with the email id '. $agencyemail.'.';
 						else:
 								#set the data to create
	 						$password=$this->generateRandomString();
	 						$allowedbusiness=$request['allowedbusiness'];
 						    $data['User']['usertype'] = 'reseller';
							$data['User']['firstname'] = $request['firstname'];
							$data['User']['lastname'] = $request['lastname'];
							$data['User']['email'] = $request['email'];
							$data['User']['allowedbusiness']=$allowedbusiness;
							if($request['payment_status']==1){
								$data['User']['status']=1;	
							}
							else{
								$data['User']['status']=0;
							}
							
							$data['User']['password'] =$password;
							$data['User']['createdat'] = date('Y-m-d H:i:s');
							if($this->User->save($data)){ //Create Agency user 
					 			 $id = $this->User->getLastInsertId();
					 			 $response['AgencyId']=$id;
					 				$info['AgencysiteSetting']['user_id'] = $id;
					 				$info['AgencysiteSetting']['agencyname'] = $request['agencyname'];
					 				$info['AgencysiteSetting']['addressline1']=$request['addressline1'];
					 				$info['AgencysiteSetting']['addressline2']=$request['addressline2'];
					 				$info['AgencysiteSetting']['city_id']=$request['city'];
					 				$info['AgencysiteSetting']['zip']=$request['zip'];
					 				$info['AgencysiteSetting']['phone']=$request['phone'];
					 				$info['AgencysiteSetting']['sitetitle']=$request['agencyname'];
					 				

					 				if($this->AgencysiteSetting->save($info))
					 				{ 
					 					$response['success']=1;
					 						$response['message']='Agency setup successfully!!!';
					 					$response['agency']=$data;
					 			   }
					 			   //Sending Email to Agency user
					 			   $HTML='';					 			   
					 				$HTML.='Dear '.$request['firstname'].' '.$request['lastname'].',';
					 			   $HTML.='<br> Email: '.$request['email'];
					 			   $HTML.='<br> Password: '.$password;
					 			   	$response['emailtemplate']=$HTML;
			 						$email_template=$HTML;
									$from='mss.yogendra@gmail.com';
									$to=$data['email'];
									$subject='New Agency User ';
									if(!$this->sendEmail($email_template,$from,$to,$subject)){
											$response['notification']='Email notification send to your email id.';
										}
										else{
											$response['notification']='Email notification not send please contact with your admin!!!';
										}
					 			}
 						endif;
					endif;
			endif;
			echo json_encode($response);
			die;
		}

		public function _validateinput($param)
		{
			if(empty($param['firstname'])){
				$this->error[]='firstname should not be empty';
			}
			if(empty($param['lastname'])){
				$this->error[]='lastname should not be empty';
			}
			if(empty($param['email'])){

				$this->error[]='email is required.';
			}
			else{
				if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", trim($param['email']))){
					$this->error[]='email is not valid.';
				}
			}


			if(empty($param['agencyname'])){
				$this->error[]='agencyname should not be empty';
			}		
			if(empty($param['addressline1'])){
				$this->error[]='addressline1 should not be empty';
			}


		}
		
		##########################Get Agency Payment Status############################
		###						Request parameter 										
		###					      1. token 
		####   					  2. email
		####				      3. paystatus				
		###############################################################################		
		public function Agencypayment_Status()
		{
			$response=array();
			$request=$this->request->data;
			if(empty(@$request['token']) || !$this->validateToken(@$request['token'])){
				http_response_code(401); exit( 'Not authorized' );
			}
			if(isset($request['email']) && isset($request['paystatus'])):
				$agencyemail=trim($request['email']);
				$paystatus=trim($request['paystatus']);
				if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $agencyemail)):
						$response['success']=0;
						$response['message']='Please enter valid email id.....';
				else:
						$this->loadModel('User');
						$this->loadModel('AgencysiteSetting');
						$agencydata= $this->User->find('first',array('conditions'=>array('User.email'=>@$agencyemail,'User.usertype'=>'reseller'),'recursive'=>-1));
						if(count($agencydata)>0):
	 							//user already exists with the current email id
								//pr($agencydata);
								$agencydata['User']['status']=(int)$paystatus;
								//$agencydata['User']['status']=(int)$paystatus;
								//pr($agencydata);
								try {
									$this->User->save($agencydata);
									if($agencydata['User']['status']==0):
									//Disable the Agency user
										$response['success']=1;
										$response['message']='Agency Disabled due to payment..';
									else:
										//Enable the Agency users
										$response['success']=1;
										$response['message']='Agency Enabled successfully!!!';
									endif;
								} catch (Exception $e) {
									  $response['success']=0;
										$response['message']=$e->getMessage().'There are some issue occured, Please try later!!!';
								}

	 					else:
	 							$response['success']=0;
								$response['message']='No Agency is exists with this email id '. $agencyemail;
	 					endif;
				endif;
			else:
					$response['success']=0;
				$response['message']='parameter is missing!!!';
			endif;
			echo json_encode($response);
			die;
		}
		##########################Get Agency count############################
		###						Request parameter 										
		###					      1. token 
		####   					  2. AgencyEmail					
		###############################################################################	

		public function getAgencycount()
		{
			$response=array();
			$request=$this->request->data;
			if(empty(@$request['token']) || !$this->validateToken(@$request['token'])){
				http_response_code(401); exit( 'Not authorized' );
			}
			if(isset($request['AgencyEmail'])):
					$agencyemail=trim($request['AgencyEmail']);
					#$paystatus=trim($request['paystatus']);
					if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $agencyemail)):
							$response['success']=0;
							$response['message']='Please enter valid email id.....';
					else:
							$this->loadModel('User');
							$this->loadModel('AgencysiteSetting');
							$this->loadModel('Business');
							$agencydata= $this->User->find('first',array('conditions'=>array('User.email'=>@$agencyemail,'User.usertype'=>'reseller'),'recursive'=>-1));
							if(count($agencydata)>0):
		 							//user already exists with the current email id
									$agencyId=$agencydata['User']['id'];
									$businesses = $this->Business->find('all',array('conditions'=>array('Business.agency_id'=>$agencyId),'fields'=>array('Business.id','businessname'),'recursive'=>0));
									$response['businesses']=$businesses;
									$response['TotalBusiness']=count($businesses);
		 					else:
		 							$response['success']=0;
									$response['message']='No Agency is exists with this email id '. $agencyemail;
		 					endif;
					endif;
			else:
				$response['success']=0;
				$response['message']='parameter is missing!!!';
			endif;
			echo json_encode($response);
			die;
		}																																											
}

?>
