<?php

namespace JeroenDesloovere\LinkedIn;

/**
 * LinkedIn API
 *
 * This LinkedIn API PHP Wrapper class connects to the LinkedIn API.
 *
 * @author Jeroen Desloovere <info@jeroendesloovere.be>
 */
class LinkedIn
{
    // API url
    const API_URL = 'https://api.linkedin.com/';

    // API version
    const API_VERSION = 'v1';

    // current version
    const VERSION = '0.0.1';

    /**
     * App key
     *
     * @var string
     */
    private $appKey;

    /**
     * App secret
     *
     * @var string
     */
    private $appSecret;

    /**
     * OAuth acces token
     *
     * @var string
     */
    private $oAuthAccessToken;

    /**
     * Construct
     *
     * @param string $appKey
     * @param string $appSecret
     * @param string $oAuthAccessToken
     * @param string $tokenSecret
     */
    public function __construct($appKey, $appSecret, $oAuthAccessToken)
    {
        $this->appKey = (string) $appKey;
        $this->appSecret = (string) $appSecret;
        $this->oAuthAccessToken = (string) $oAuthAccessToken;
    }

    /**
     * Do call
     *
     * @param string $url The URL to call.
     * @param array[optional] $data The data to pass.
     */
    protected function doCall($url, $data = null)
    {
        error_reporting(-1);
        ini_set('display_errors', 'On');
        $signedUrl = self::API_URL . self::API_VERSION . '/' . $url;
        // get the signed URL
        $signedUrl = $signedUrl . '&format=json';
        //echo $signedUrl;
        $ch = curl_init();  
        curl_setopt($ch,CURLOPT_URL,$signedUrl);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

        $result=curl_exec($ch);
        // close connection
        curl_close($ch);
        // handle LinkedIn response data
       return(json_decode($result));
    }

    /**
     * Get profile
     */
    public function getProfile()
    {   

        return $this->doCall('people/~');
    }

    /**
     * Find comany
     */
    public function findCompany($keywords) 
    {
        return $this->doCall('company-search?keywords='. $keywords . '&oauth2_access_token=' . $this->oAuthAccessToken);
    }

    /**
     * Get company details
     */
    public function getCompanyDetials($companyId)
    {
        return $this->doCall('companies/' . $companyId . ':(id,name,ticker,description,universal-name,email-domains,company-type,website-url,industries,status,logo-url,locations)' . '?oauth2_access_token=' . $this->oAuthAccessToken);
    }
}

/**
 * LinkedIn API Exception
 *
 * @author Jeroen Desloovere <info@jeroendesloovere.be>
 */
class LinkedInException extends \Exception {}
